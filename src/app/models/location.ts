enum LocationType {
  Office = 1,
  User
}

export interface OfficeLocation{
  id?: string,
  coords?:{
    latitude?:number,
    longitude?:number
  },
  location_ID?: string,
  name?:string,
  hasAccess: boolean
}