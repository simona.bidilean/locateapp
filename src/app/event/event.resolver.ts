import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { EventUserRightsService } from '../services/event-user-rights.service';
import * as CONSTANTS from '../constants/constants';

@Injectable()
export class EventResolver implements Resolve<any> {

  constructor(private eventUserRightsService: EventUserRightsService ) {}

  resolve() {
    // return StorageService.get(CONSTANTS.EVENTS);
  }
}
