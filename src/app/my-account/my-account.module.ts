import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MyAccountPageRoutingModule } from './my-account-routing.module';
import { UserSessionResolver } from '../resolver/user.session.resolver';
import { MyAccountPage } from './my-account.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyAccountPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MyAccountPage],
  providers: [
    UserSessionResolver
  ]
})
export class MyAccountPageModule {}
