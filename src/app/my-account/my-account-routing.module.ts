import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyAccountPage } from './my-account.page';
import { UserSessionResolver } from '../resolver/user.session.resolver';
const routes: Routes = [
  {
    path: '',
    component: MyAccountPage,
    resolve: {
      data: UserSessionResolver
    }
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyAccountPageRoutingModule {}
