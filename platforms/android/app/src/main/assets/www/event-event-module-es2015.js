(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["event-event-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/event/event.page.html":
/*!*****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/event/event.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Events</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n\r\n  \r\n  <ion-list>\r\n    <ion-item *ngFor=\"let event of events\">\r\n \r\n  <ion-label>   <h5>Status: {{event.status}}  </h5> </ion-label> \r\n  <ion-label>  <h5>Location: {{locationService.getCompanyNameById(event.location_ID, userSession.locations)}} </h5>  </ion-label> \r\n  <ion-label>  <h5>User:  {{userService.getUserNameById(event.user_ID, userSession.users)}}   </h5> </ion-label>\r\n\r\n  <ion-button (click)=\"allow(event)\" *ngIf=\"event.status!='resolved'\" color=\"success\" expand=\"block\" size=\"small\"> Allow </ion-button>\r\n  <ion-button (click)=\"deny(event)\" *ngIf=\"event.status!='resolved'\" color=\"danger\" expand=\"block\" size=\"small\"> Deny </ion-button>\r\n  <ion-button (click)=\"revoke(event)\" *ngIf=\"event.status!='waiting'\" color=\"danger\" expand=\"block\" size=\"small\"> Revoke </ion-button>\r\n\r\n    </ion-item>\r\n  </ion-list>\r\n\r\n \r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-row>\r\n    <ion-col>\r\n  <ion-button routerLink=\"/home\" routerDirection=\"root\" color=\"dark\">\r\n   Go back\r\n  </ion-button>\r\n</ion-col>\r\n</ion-row>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/event/event-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/event/event-routing.module.ts ***!
  \***********************************************/
/*! exports provided: EventPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventPageRoutingModule", function() { return EventPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _event_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./event.page */ "./src/app/event/event.page.ts");




const routes = [
    {
        path: '',
        component: _event_page__WEBPACK_IMPORTED_MODULE_3__["EventPage"]
    }
];
let EventPageRoutingModule = class EventPageRoutingModule {
};
EventPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EventPageRoutingModule);



/***/ }),

/***/ "./src/app/event/event.module.ts":
/*!***************************************!*\
  !*** ./src/app/event/event.module.ts ***!
  \***************************************/
/*! exports provided: EventPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventPageModule", function() { return EventPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _event_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./event-routing.module */ "./src/app/event/event-routing.module.ts");
/* harmony import */ var _event_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./event.page */ "./src/app/event/event.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");









let EventPageModule = class EventPageModule {
};
EventPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild([
                {
                    path: '/event',
                    component: _event_page__WEBPACK_IMPORTED_MODULE_7__["EventPage"],
                    resolve: {
                        UserSessionResolver: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
                    }
                }
            ]),
            _event_routing_module__WEBPACK_IMPORTED_MODULE_6__["EventPageRoutingModule"]
        ],
        declarations: [_event_page__WEBPACK_IMPORTED_MODULE_7__["EventPage"]],
        providers: [
            _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
        ]
    })
], EventPageModule);



/***/ }),

/***/ "./src/app/event/event.page.scss":
/*!***************************************!*\
  !*** ./src/app/event/event.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2ZW50L2V2ZW50LnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/event/event.page.ts":
/*!*************************************!*\
  !*** ./src/app/event/event.page.ts ***!
  \*************************************/
/*! exports provided: EventPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventPage", function() { return EventPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/event-user-rights.service */ "./src/app/services/event-user-rights.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_notifications_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/notifications.service */ "./src/app/services/notifications.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");










let EventPage = class EventPage {
    constructor(eventUserRightsService, userRightsService, db, notification, userService, router, route, locationService, eventPageResolver) {
        this.eventUserRightsService = eventUserRightsService;
        this.userRightsService = userRightsService;
        this.db = db;
        this.notification = notification;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.locationService = locationService;
        this.eventPageResolver = eventPageResolver;
        this.eventUserRightsService.getEventUserRights().subscribe(events => {
            this.events = events;
            console.log(events);
        });
        this.route.data.subscribe(session => {
            this.userSession = session.data;
        });
        this.eventPageResolver.resolve().then(_response => {
            this.userSession = _response;
        });
    }
    allow(userEvent) {
        debugger;
        let _right;
        for (let i = 0; i < this.userSession.usersRight.length; i++) {
            if (this.userSession.usersRight[i].user_ID == userEvent.user_ID)
                _right = this.userSession.usersRight[i];
            console.log(_right);
        }
        if (!_right || _right == null || typeof (_right) == undefined || _right == undefined) {
            let right = {
                'location_ID': userEvent.location_ID,
                'user_ID': userEvent.user_ID,
                'permission': true
            };
            this.userRightsService.addUserRight(right);
        }
        else {
            let right_permission = {
                'permission': true
            };
            this.userRightsService.updateUserRight(_right.id, right_permission);
        }
        let event = {
            status: 'resolved'
        };
        this.eventUserRightsService.updatetEventUserRight(userEvent.id, event);
        let title = "Access granted";
        let message = "Hey " + this.userService.getUserNameById(userEvent.user_ID, this.userSession.users) + ", admin granted you access to the office!";
        this.notification.send(userEvent.user_ID, title, message).subscribe(() => {
        });
    }
    deny(userEvent) {
        debugger;
        let _right;
        for (let i = 0; i < this.userSession.usersRight.length; i++) {
            if (this.userSession.usersRight[i].user_ID == userEvent.user_ID)
                _right = this.userSession.usersRight[i];
            console.log(_right);
        }
        if (!_right || _right == null || typeof (_right) == undefined || _right == undefined) {
            let right = {
                'location_ID': userEvent.location_ID,
                'user_ID': userEvent.user_ID,
                'permission': false
            };
            this.userRightsService.addUserRight(right);
        }
        else {
            let right_permission = {
                'permission': false
            };
            this.userRightsService.updateUserRight(_right.id, right_permission);
        }
        let event = {
            status: 'resolved'
        };
        this.eventUserRightsService.updatetEventUserRight(userEvent.id, event);
        let title = "Access denied";
        let message = "Hey " + this.userService.getUserNameById(userEvent.user_ID, this.userSession.users) + ", admin denied you access to the office!";
        this.notification.send(userEvent.user_ID, title, message).subscribe(() => {
        });
    }
    revoke(userEvent) {
        let event = {
            status: 'waiting'
        };
        this.eventUserRightsService.updatetEventUserRight(userEvent.id, event);
    }
};
EventPage.ctorParameters = () => [
    { type: _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_2__["EventUserRightsService"] },
    { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_3__["UserRightsService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _services_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
    { type: _services_location_service__WEBPACK_IMPORTED_MODULE_7__["LocationService"] },
    { type: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__["UserSessionResolver"] }
];
EventPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-event',
        template: __webpack_require__(/*! raw-loader!./event.page.html */ "./node_modules/raw-loader/index.js!./src/app/event/event.page.html"),
        styles: [__webpack_require__(/*! ./event.page.scss */ "./src/app/event/event.page.scss")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_2__["EventUserRightsService"],
        _services_user_rights_service__WEBPACK_IMPORTED_MODULE_3__["UserRightsService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _services_notifications_service__WEBPACK_IMPORTED_MODULE_5__["NotificationsService"],
        _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
        _services_location_service__WEBPACK_IMPORTED_MODULE_7__["LocationService"],
        _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__["UserSessionResolver"]])
], EventPage);



/***/ })

}]);
//# sourceMappingURL=event-event-module-es2015.js.map