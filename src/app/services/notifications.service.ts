import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Platform } from '@ionic/angular';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  constructor(
    private http: HttpClient
  ) { }

  send(topic, title, message) {
    let body = { "to": "/topics/" + topic, "priority": "high", "notification": { "body": message, "title": title, } };
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'key=AAAAC_6mwDg:APA91bFOkRg9nCKLTjbM7IWVCa3qpG3f-NWNhH0Fw664wtaLBRfteabx2XczP4BuAafKzjESJAQOy5bD5x_J0hGjDfIDc64EH2H1VInWT9HDxPyEGDBhZwa13IwYlOofJnb2whsEKwBS' })
    };
    return this.http.post("https://fcm.googleapis.com/fcm/send", body, httpOptions);
  }
}