(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-edit-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/edit/edit.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/edit/edit.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Edit Page</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <div [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit(profileForm)\">\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">First Name</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"firstName\" value={{currentUser.firstName}} ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Last Name</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"lastName\" value={{currentUser.lastName}} ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Phone</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"phone\" value={{currentUser.phone}} ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Address</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"address\" value={{currentUser.address}} ></ion-input>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-label position=\"floating\" color=\"primary\">Function</ion-label>\r\n      <ion-input type=\"text\" formControlName=\"function\" value={{currentUser.function}} ></ion-input>\r\n    </ion-item>\r\n\r\n  \r\n    <ion-button class=\"submit-button\" color=\"success\" type=\"submit\" (click)=\"onSubmit()\" >Save</ion-button>\r\n    <ion-button routerLink=\"/my-account\" routerDirection=\"root\" color=\"danger\"> Go Back </ion-button>\r\n\r\n  </div>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/edit/edit-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/edit/edit-routing.module.ts ***!
  \*********************************************/
/*! exports provided: EditPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPageRoutingModule", function() { return EditPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _edit_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./edit.page */ "./src/app/edit/edit.page.ts");





var routes = [
    {
        path: '',
        component: _edit_page__WEBPACK_IMPORTED_MODULE_4__["EditPage"],
        resolve: {
            data: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_3__["UserSessionResolver"]
        }
    }
];
var EditPageRoutingModule = /** @class */ (function () {
    function EditPageRoutingModule() {
    }
    EditPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], EditPageRoutingModule);
    return EditPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/edit/edit.module.ts":
/*!*************************************!*\
  !*** ./src/app/edit/edit.module.ts ***!
  \*************************************/
/*! exports provided: EditPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPageModule", function() { return EditPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _edit_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-routing.module */ "./src/app/edit/edit-routing.module.ts");
/* harmony import */ var _edit_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit.page */ "./src/app/edit/edit.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var EditPageModule = /** @class */ (function () {
    function EditPageModule() {
    }
    EditPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild([
                    {
                        path: '/edit',
                        component: _edit_page__WEBPACK_IMPORTED_MODULE_7__["EditPage"],
                        resolve: {
                            UserSessionResolver: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_5__["UserSessionResolver"]
                        }
                    }
                ]),
                _edit_routing_module__WEBPACK_IMPORTED_MODULE_6__["EditPageRoutingModule"]
            ],
            declarations: [_edit_page__WEBPACK_IMPORTED_MODULE_7__["EditPage"]],
            providers: [
                _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_5__["UserSessionResolver"]
            ]
        })
    ], EditPageModule);
    return EditPageModule;
}());



/***/ }),

/***/ "./src/app/edit/edit.page.scss":
/*!*************************************!*\
  !*** ./src/app/edit/edit.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXQvZWRpdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/edit/edit.page.ts":
/*!***********************************!*\
  !*** ./src/app/edit/edit.page.ts ***!
  \***********************************/
/*! exports provided: EditPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPage", function() { return EditPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");












var EditPage = /** @class */ (function () {
    function EditPage(router, route, alertController, userService, navCtrl, authService, formBuilder, db, editPageResolver) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.alertController = alertController;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.db = db;
        this.editPageResolver = editPageResolver;
        this.editPageResolver.resolve().then(function (_response) {
            _this.userSession = _response;
            _this.currentUser = _this.userSession.currentUser;
            console.log(_this.currentUser);
        });
        this.route.data.subscribe(function (session) {
            _this.userSession = session.data;
            _this.currentUser = _this.userSession.currentUser;
            console.log(_this.currentUser);
        });
        this.profileForm = this.formBuilder.group({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](),
            function: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]()
        });
    }
    EditPage.prototype.onSubmit = function () {
        var _this = this;
        debugger;
        this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_6__["USER_SESSION"]).then(function (_userSession) {
            _userSession.currentUser.firstName = _this.profileForm.value.firstName;
            _userSession.currentUser.lastName = _this.profileForm.value.lastName;
            _userSession.currentUser.phone = _this.profileForm.value.phone;
            _userSession.currentUser.address = _this.profileForm.value.address;
            _userSession.currentUser.function = _this.profileForm.value.function;
            _this.db.set(_constants_constants__WEBPACK_IMPORTED_MODULE_6__["USER_SESSION"], _userSession).then(function () {
                _this.userService.updateUser(_this.currentUser.uid, _userSession.currentUser).then(function () {
                    _this.router.navigate(['/my-account']);
                });
            });
        });
    };
    EditPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
        { type: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__["UserSessionResolver"] }
    ]; };
    EditPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! raw-loader!./edit.page.html */ "./node_modules/raw-loader/index.js!./src/app/edit/edit.page.html"),
            styles: [__webpack_require__(/*! ./edit.page.scss */ "./src/app/edit/edit.page.scss")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
            _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_9__["UserSessionResolver"]])
    ], EditPage);
    return EditPage;
}());



/***/ }),

/***/ "./src/app/resolver/user.session.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/resolver/user.session.resolver.ts ***!
  \***************************************************/
/*! exports provided: UserSessionResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSessionResolver", function() { return UserSessionResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");











var UserSessionResolver = /** @class */ (function () {
    function UserSessionResolver(db, geolocation, userService, locationService, userRightsService, events) {
        this.db = db;
        this.geolocation = geolocation;
        this.userService = userService;
        this.locationService = locationService;
        this.userRightsService = userRightsService;
        this.events = events;
    }
    UserSessionResolver.prototype.resolve = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_4__["USER_SESSION"]).then(function (_userSession) {
                if (_userSession != null) {
                    if (_userSession.currentUser != null) {
                        if (_userSession.currentUser.id != null) {
                            _this.userService.getUser(_userSession.currentUser.id).subscribe(function (userDetail) {
                                _userSession.currentUser = userDetail;
                                _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(function () {
                                    if (!_userSession.currentUser.isAdmin) {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.geolocation.getCurrentPosition(), _this.locationService.getLocations(), _this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var mylocation = _a[0], locations = _a[1], userRights = _a[2];
                                            return ({ mylocation: mylocation, locations: locations, userRights: userRights });
                                        })).subscribe(function (response) {
                                            _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                                            _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;
                                            _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                                            delete response['mylocation'];
                                            response['users'] = [_userSession.currentUser];
                                            response['usersRight'] = [];
                                            response['usersRight'].push(response['userRights']);
                                            delete response['userRights'];
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    var _permission = response['usersRight'].filter(function (accessRight) { return accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id; })[0].permission;
                                                    if (_permission) {
                                                        _location.hasAccess = _permission;
                                                    }
                                                    else if (!_permission || !response['usersRight']) {
                                                        _location.hasAccess = false;
                                                    }
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.locationService.getLocations(), _this.userService.getUsers(), _this.userRightsService.getUserRights()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var locations = _a[0], users = _a[1], usersRight = _a[2];
                                            return ({ locations: locations, users: users, usersRight: usersRight });
                                        })).subscribe(function (response) {
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    _location.hasAccess = _this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
            });
        });
    };
    UserSessionResolver.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
        { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"] }
    ]; };
    UserSessionResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
            _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
            _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"]])
    ], UserSessionResolver);
    return UserSessionResolver;
}());



/***/ })

}]);
//# sourceMappingURL=edit-edit-module-es5.js.map