import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserSessionResolver } from '../resolver/user.session.resolver';
import { EditPage } from './edit.page';

const routes: Routes = [
  {
    path: '',
    component: EditPage,
    resolve: {
      data: UserSessionResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPageRoutingModule {}
