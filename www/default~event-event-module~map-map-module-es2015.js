(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~event-event-module~map-map-module"],{

/***/ "./src/app/resolver/user.session.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/resolver/user.session.resolver.ts ***!
  \***************************************************/
/*! exports provided: UserSessionResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSessionResolver", function() { return UserSessionResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");











let UserSessionResolver = class UserSessionResolver {
    constructor(db, geolocation, userService, locationService, userRightsService, events) {
        this.db = db;
        this.geolocation = geolocation;
        this.userService = userService;
        this.locationService = locationService;
        this.userRightsService = userRightsService;
        this.events = events;
    }
    resolve() {
        return new Promise((resolve) => {
            this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_4__["USER_SESSION"]).then(_userSession => {
                if (_userSession != null) {
                    if (_userSession.currentUser != null) {
                        if (_userSession.currentUser.id != null) {
                            this.userService.getUser(_userSession.currentUser.id).subscribe(userDetail => {
                                _userSession.currentUser = userDetail;
                                this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(() => {
                                    if (!_userSession.currentUser.isAdmin) {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(this.geolocation.getCurrentPosition(), this.locationService.getLocations(), this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(([mylocation, locations, userRights]) => ({ mylocation, locations, userRights }))).subscribe(response => {
                                            _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                                            _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;
                                            this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                                            delete response['mylocation'];
                                            response['users'] = [_userSession.currentUser];
                                            response['usersRight'] = [];
                                            response['usersRight'].push(response['userRights']);
                                            delete response['userRights'];
                                            response['currentUser'] = _userSession.currentUser;
                                            let _updatedLocations = [];
                                            response['locations'].forEach((_location, lIndex) => {
                                                response['users'].forEach((_user, uIndex) => {
                                                    let _permission = response['usersRight'].filter((accessRight) => accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id)[0].permission;
                                                    if (_permission) {
                                                        _location.hasAccess = _permission;
                                                    }
                                                    else if (!_permission || !response['usersRight']) {
                                                        _location.hasAccess = false;
                                                    }
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(this.locationService.getLocations(), this.userService.getUsers(), this.userRightsService.getUserRights()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(([locations, users, usersRight]) => ({ locations, users, usersRight }))).subscribe(response => {
                                            response['currentUser'] = _userSession.currentUser;
                                            let _updatedLocations = [];
                                            response['locations'].forEach((_location, lIndex) => {
                                                response['users'].forEach((_user, uIndex) => {
                                                    _location.hasAccess = this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
            });
        });
    }
};
UserSessionResolver.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
    { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
    { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"] }
];
UserSessionResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
        _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
        _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
        _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"]])
], UserSessionResolver);



/***/ }),

/***/ "./src/app/services/event-user-rights.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/event-user-rights.service.ts ***!
  \*******************************************************/
/*! exports provided: EventUserRightsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventUserRightsService", function() { return EventUserRightsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");








let EventUserRightsService = class EventUserRightsService {
    constructor(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.eventUserRightCollection = this.afs.collection('eventUserRights');
        this.eventUserRights = this.eventUserRightCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(actions => {
            return actions.map(a => {
                const data = a.payload.doc.data();
                const id = a.payload.doc.id;
                return Object.assign({ id }, data);
            });
        }));
    }
    getEventUserRights() {
        return this.eventUserRights;
    }
    getEventUserRight(id) {
        return this.eventUserRightCollection.doc(id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(eventUserRight => {
            eventUserRight.id = id;
            return eventUserRight;
        }));
    }
    updatetEventUserRight(id, event) {
        return this.eventUserRightCollection.doc(id).update(event);
    }
    addOrUpdateEventUserRight(eventUserRight) {
        return new Promise((resolve) => {
            this.eventUserRights.subscribe(_currentEvents => {
                let _userEvents = _currentEvents.filter(requestEvent => requestEvent.user_ID === eventUserRight.user_ID);
                if (_userEvents.length > 0) {
                    let _userEvent = _userEvents[0];
                    this.updatetEventUserRight(_userEvent.id, eventUserRight);
                    resolve("Updated");
                }
                else {
                    this.eventUserRightCollection.add(eventUserRight);
                    resolve("Added");
                }
            });
        });
    }
    addEventUserRight(eventUserRight) {
        return this.eventUserRightCollection.add(eventUserRight);
    }
    deleteEventUserRight(id) {
        return this.eventUserRightCollection.doc(id).delete();
    }
};
EventUserRightsService.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"] }
];
EventUserRightsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
], EventUserRightsService);



/***/ }),

/***/ "./src/app/services/notifications.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/notifications.service.ts ***!
  \***************************************************/
/*! exports provided: NotificationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsService", function() { return NotificationsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let NotificationsService = class NotificationsService {
    constructor(http) {
        this.http = http;
    }
    send(topic, title, message) {
        let body = { "to": "/topics/" + topic, "priority": "high", "notification": { "body": message, "title": title, } };
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json', 'Authorization': 'key=AAAAC_6mwDg:APA91bFOkRg9nCKLTjbM7IWVCa3qpG3f-NWNhH0Fw664wtaLBRfteabx2XczP4BuAafKzjESJAQOy5bD5x_J0hGjDfIDc64EH2H1VInWT9HDxPyEGDBhZwa13IwYlOofJnb2whsEKwBS' })
        };
        return this.http.post("https://fcm.googleapis.com/fcm/send", body, httpOptions);
    }
};
NotificationsService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
NotificationsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], NotificationsService);



/***/ })

}]);
//# sourceMappingURL=default~event-event-module~map-map-module-es2015.js.map