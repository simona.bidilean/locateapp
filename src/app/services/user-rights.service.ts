import { Injectable } from '@angular/core';
import 'firebase/auth';
import 'firebase/firestore';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/storage';
import { UserRight } from '../models/userRight';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserRightsService {
  private userRights: Observable<UserRight[]>;
  private userRightCollection: AngularFirestoreCollection<UserRight>;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {

    this.userRightCollection = this.afs.collection<UserRight>('userRights');
    this.userRights = this.userRightCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getUserRights(): Observable<UserRight[]> {
    return this.userRights;
  }

  getUserRight(id: string): Observable<UserRight> {
    return this.userRightCollection.doc<UserRight>(id).valueChanges().pipe(
      take(1),
      map(userRight => {
        userRight.id = id;
        return userRight
      })
    );
  }


  getUserRightsByUserId(userId: string): Observable<UserRight> {
    return this.userRights.pipe(map(userRights => {
      let ur = userRights.filter(userRight => userRight.user_ID === userId);
      return (ur.length > 0) ? ur[0] : null;
    }));

  }


  updateUserRight(id: string, userRight: UserRight) {

    return this.userRightCollection.doc(id).update(userRight);

  }

  addUserRight(userRight: UserRight): Promise<DocumentReference> {
    return this.userRightCollection.add(userRight);
  }

  deleteUserRight(id: string): Promise<void> {
    return this.userRightCollection.doc(id).delete();
  }

  addOrUpdateUserRight(userRight: UserRight): Promise<any> {
    return new Promise<any>((resolve) => {
      this.userRights.subscribe(_currentRights => {
        let _userRights = _currentRights.filter(requestEvent => requestEvent.user_ID === userRight.user_ID);
        if (_userRights.length > 0) {
          let _userEvent = _userRights[0];
          this.updateUserRight(_userEvent.id, userRight);
          resolve("Updated");
        } else {
          this.userRightCollection.add(userRight);
          resolve("Added");
        }
      });
    });
  }

 




}