(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/Home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/Home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n \r\n  <ion-toolbar>\r\n    <ion-title>Home</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n \r\n<ion-content padding>\r\n \r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col text-center>\r\n         <ion-icon name=\"ios-contact\"  id=\"person\"></ion-icon> <br>\r\n      <h4>  Welcome back,<br> {{currentUser.firstName}} !</h4>\r\n    </ion-col>\r\n    </ion-row>\r\n  </ion-grid>  \r\n\r\n  <ion-row>\r\n      <ion-col col-12>\r\n        <ion-card padding>\r\n           <a routerLink=\"/my-account\" routerDirection=\"root\"><ion-icon ios=\"ios-person\" md=\"md-person\" id=\"person2\"></ion-icon> </a> \r\n          <h2>My Account</h2>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col col-12>\r\n      <ion-card padding>\r\n <a routerLink=\"/map\" routerDirection=\"root\">  <ion-icon name=\"map\" id=\"map\" ></ion-icon>  </a>\r\n        <h2>Map</h2>  \r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  \r\n  <ion-row *ngIf=\"currentUser.isAdmin\">\r\n    <ion-col col-12>\r\n      <ion-card padding>\r\n <a routerLink=\"/event\" routerDirection=\"root\">    <ion-icon name=\"alert\" id=\"alert\" ></ion-icon> </a>\r\n        <h2>Events</h2>  \r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row> \r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-row>\r\n    <ion-col text-center>\r\n  <ion-button (click)=\"logout()\" color=\"dark\">\r\n    Log out\r\n  </ion-button>\r\n</ion-col>\r\n</ion-row>\r\n</ion-footer>\r\n "

/***/ }),

/***/ "./src/app/home/Home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/Home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#person {\n  width: 100px;\n  height: 100px;\n}\n\n#person2 {\n  width: 60px;\n  height: 60px;\n}\n\n#people {\n  width: 60px;\n  height: 60px;\n}\n\n#map {\n  width: 60px;\n  height: 60px;\n}\n\n#alert {\n  width: 60px;\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXHNpbW9uYS5iaWRpbGVhblxcRG9jdW1lbnRzXFxsb2NhdGVBcHBcXGxvY2F0ZWFwcC9zcmNcXGFwcFxcaG9tZVxcSG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvSG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREdBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQUo7O0FER0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvSG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjcGVyc29ue1xyXG4gICAgd2lkdGg6IDEwMHB4IDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbn1cclxuXHJcblxyXG4jcGVyc29uMntcclxuICAgIHdpZHRoOiA2MHB4IDtcclxuICAgIGhlaWdodDogNjBweDtcclxufVxyXG5cclxuI3Blb3BsZXtcclxuICAgIHdpZHRoOiA2MHB4IDtcclxuICAgIGhlaWdodDogNjBweDtcclxufVxyXG5cclxuI21hcHtcclxuICAgIHdpZHRoOiA2MHB4IDtcclxuICAgIGhlaWdodDogNjBweDtcclxufVxyXG5cclxuI2FsZXJ0e1xyXG4gICAgd2lkdGg6IDYwcHggO1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG59XHJcblxyXG5cclxuXHJcbiIsIiNwZXJzb24ge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG59XG5cbiNwZXJzb24yIHtcbiAgd2lkdGg6IDYwcHg7XG4gIGhlaWdodDogNjBweDtcbn1cblxuI3Blb3BsZSB7XG4gIHdpZHRoOiA2MHB4O1xuICBoZWlnaHQ6IDYwcHg7XG59XG5cbiNtYXAge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xufVxuXG4jYWxlcnQge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");





var routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
        resolve: {
            data: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
        }
    }
];
var HomePageRoutingModule = /** @class */ (function () {
    function HomePageRoutingModule() {
    }
    HomePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], HomePageRoutingModule);
    return HomePageRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");








var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]],
            providers: [
                _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_7__["UserSessionResolver"]
            ]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/fcm/ngx */ "./node_modules/@ionic-native/fcm/ngx/index.js");










var HomePage = /** @class */ (function () {
    function HomePage(router, route, authService, db, fcm) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.authService = authService;
        this.db = db;
        this.fcm = fcm;
        this.route.data.subscribe(function (session) {
            _this.userSession = session.data;
            _this.currentUser = _this.userSession.currentUser;
        });
        this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_3__["USER_SESSION"]).then(function (_userSession) {
            if (_this.currentUser.isAdmin) {
                _this.fcm.subscribeToTopic(_constants_constants__WEBPACK_IMPORTED_MODULE_3__["REQUEST_ACCESS_TOPIC"]).then(function () {
                    console.log("Admin receives push notifications when access is requested!");
                });
            }
            else if (!_this.currentUser.isAdmin) {
                _this.fcm.subscribeToTopic(_this.currentUser.uid).then(function () {
                    console.log("User receives push notifications when access is granted or denied!");
                });
            }
        });
    }
    HomePage.prototype.logout = function () {
        var _this = this;
        this.authService.logoutUser(this.currentUser)
            .then(function (res) {
            console.log(res);
            _this.db.clear().then(function () {
                _this.router.navigate(['/']);
            });
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticateService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
        { type: _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-Home',
            template: __webpack_require__(/*! raw-loader!./Home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/Home.page.html"),
            styles: [__webpack_require__(/*! ./Home.page.scss */ "./src/app/home/Home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticateService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
            _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"]])
    ], HomePage);
    return HomePage;
}());



/***/ }),

/***/ "./src/app/resolver/user.session.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/resolver/user.session.resolver.ts ***!
  \***************************************************/
/*! exports provided: UserSessionResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSessionResolver", function() { return UserSessionResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");











var UserSessionResolver = /** @class */ (function () {
    function UserSessionResolver(db, geolocation, userService, locationService, userRightsService, events) {
        this.db = db;
        this.geolocation = geolocation;
        this.userService = userService;
        this.locationService = locationService;
        this.userRightsService = userRightsService;
        this.events = events;
    }
    UserSessionResolver.prototype.resolve = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_4__["USER_SESSION"]).then(function (_userSession) {
                if (_userSession != null) {
                    if (_userSession.currentUser != null) {
                        if (_userSession.currentUser.id != null) {
                            _this.userService.getUser(_userSession.currentUser.id).subscribe(function (userDetail) {
                                _userSession.currentUser = userDetail;
                                _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(function () {
                                    if (!_userSession.currentUser.isAdmin) {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.geolocation.getCurrentPosition(), _this.locationService.getLocations(), _this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var mylocation = _a[0], locations = _a[1], userRights = _a[2];
                                            return ({ mylocation: mylocation, locations: locations, userRights: userRights });
                                        })).subscribe(function (response) {
                                            _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                                            _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;
                                            _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                                            delete response['mylocation'];
                                            response['users'] = [_userSession.currentUser];
                                            response['usersRight'] = [];
                                            response['usersRight'].push(response['userRights']);
                                            delete response['userRights'];
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    var _permission = response['usersRight'].filter(function (accessRight) { return accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id; })[0].permission;
                                                    if (_permission) {
                                                        _location.hasAccess = _permission;
                                                    }
                                                    else if (!_permission || !response['usersRight']) {
                                                        _location.hasAccess = false;
                                                    }
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.locationService.getLocations(), _this.userService.getUsers(), _this.userRightsService.getUserRights()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var locations = _a[0], users = _a[1], usersRight = _a[2];
                                            return ({ locations: locations, users: users, usersRight: usersRight });
                                        })).subscribe(function (response) {
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    _location.hasAccess = _this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
            });
        });
    };
    UserSessionResolver.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
        { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"] }
    ]; };
    UserSessionResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
            _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
            _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"]])
    ], UserSessionResolver);
    return UserSessionResolver;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map