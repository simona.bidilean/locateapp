import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import * as CONSTANTS from '../constants/constants';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { UserService } from '../services/user.service';
import { LocationService } from '../services/location.service';
import { UserRightsService } from '../services/user-rights.service';
import { OfficeLocation } from '../models/location';
import { User } from 'firebase';
import { UserRight } from '../models/userRight';
import { Events } from '@ionic/angular';


@Injectable()
export class UserSessionResolver implements Resolve<any> {

  constructor(private db: Storage,
    private geolocation: Geolocation,
    private userService: UserService,
    private locationService: LocationService,
    private userRightsService: UserRightsService,
    public events: Events
  ) { }

  resolve() {
    return new Promise<any>((resolve) => {
      this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
        if (_userSession != null) {
          if (_userSession.currentUser != null) {
            if (_userSession.currentUser.id != null) {
              this.userService.getUser(_userSession.currentUser.id).subscribe(userDetail => {
                _userSession.currentUser = userDetail;
                this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(() => {
                  if (!_userSession.currentUser.isAdmin) {
                    combineLatest(this.geolocation.getCurrentPosition(), this.locationService.getLocations(), this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(
                      map(([mylocation, locations, userRights]: [Geoposition, OfficeLocation[], UserRight]) =>
                        ({ mylocation, locations, userRights })
                      )).subscribe(response => {
                        _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                        _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;

                        this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                        
                        delete response['mylocation'];
                        response['users'] = [_userSession.currentUser];
                        response['usersRight'] = [];
                        response['usersRight'].push(response['userRights']);
                        delete response['userRights'];
                        response['currentUser'] = _userSession.currentUser;

                        let _updatedLocations = [];
                        response['locations'].forEach((_location, lIndex) => {
                          response['users'].forEach((_user, uIndex) => {
                            let _permission = response['usersRight'].filter((accessRight) => accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id)[0].permission;
                            if(_permission ){
                              _location.hasAccess = _permission;
                            } else if (!_permission || !response['usersRight']){
                              _location.hasAccess = false;
                            }
                            if (_updatedLocations.length < response['locations'].length) {
                              _updatedLocations.push(_location);
                            }

                            if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                              response['locations'] = _updatedLocations;
                              this.events.publish('user:loggedIn', response);
                              resolve(response);
                            }
                          });
                        });
                      });
                     
                  } else {
                    combineLatest(  this.locationService.getLocations(), this.userService.getUsers(), this.userRightsService.getUserRights()).pipe(
                      map(([locations, users, usersRight]: [ OfficeLocation[], User[], UserRight[]]) =>
                        ({  locations, users, usersRight })
                      )).subscribe(response => {
                        response['currentUser'] = _userSession.currentUser;
                        let _updatedLocations = [];
                        response['locations'].forEach((_location, lIndex) => {
                          response['users'].forEach((_user, uIndex) => {

                            _location.hasAccess = this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                            if (_updatedLocations.length < response['locations'].length) {
                              _updatedLocations.push(_location);
                              
                            }
                            if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                              response['locations'] = _updatedLocations;
                              this.events.publish('user:loggedIn', response);
                              resolve(response);
                            }
                          });
                        });
                      });
                  }
                });
              });
            }
          }
        }
      });
    });
  }
}
