import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import {User} from '../models/user';
import { UserService } from './user.service';
import { AngularFireAuth } from '@angular/fire/auth';
import * as CONSTANTS from '../constants/constants';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { FCM } from '@ionic-native/fcm/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
authenticationState = new BehaviorSubject(false);

  constructor(
    public afAuth: AngularFireAuth,
    private userService: UserService,
    private db:Storage,
    private fcm: FCM
    ){
     
    }
 
  registerUser(value){
   return new Promise<any>((resolve, reject) => {
     firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
     .then(
       res => resolve(res),
       err => reject(err))
   })
  }
 
  loginUser(value){
   return new Promise<any>((resolve, reject) => {
     firebase.auth().signInWithEmailAndPassword(value.email, value.password)
     .then(res => resolve(res),err => reject(err))
      this.authenticationState.next(true);
   })
  }
 
  logoutUser(user: User){
    return new Promise((resolve, reject) => {
      user.isOnline = false;
        this.db.set(CONSTANTS.CURRENT_USER, user).then(() => {
          this.userService.updateUser(user.id,user).then(() => {
            this.afAuth.auth.signOut().then(() => {

              if(user.isAdmin){
              this.fcm.unsubscribeFromTopic(CONSTANTS.REQUEST_ACCESS_TOPIC);
               } else{
              this.fcm.unsubscribeFromTopic(user.id)
                }

              this.db.clear().then(() => {
                resolve("Log out successfully!");
              });             
            });
          }).catch((error) => {
            console.log(error);
            reject();
          });
      });
      this.authenticationState.next(false);
    })
  }
  
  isAuthenticated() {
    return this.authenticationState.value;
  }

  userDetails(){
    return firebase.auth().currentUser;  
  } 
}