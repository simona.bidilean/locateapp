import { Component, OnInit, Injectable, AfterViewInit } from '@angular/core';
import { RequestAccessEvent } from '../models/request-access-event';
import { EventUserRightsService } from '../services/event-user-rights.service';
import * as CONSTANTS from '../constants/constants';
import { UserRightsService } from '../services/user-rights.service';
import { UserRight } from '../models/userRight';
import { Storage } from '@ionic/storage';
import { Events, AngularDelegate } from '@ionic/angular';
import { User } from '../models/user';
import { NotificationsService } from '../services/notifications.service';
import { UserService } from '../services/user.service';
import { LocationService } from '../services/location.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserSessionResolver } from '../resolver/user.session.resolver';


@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})

@Injectable()
export class EventPage {
  private events: RequestAccessEvent[];
  userSession: any;

  constructor(

    private eventUserRightsService: EventUserRightsService,
    private userRightsService: UserRightsService,
    private db: Storage,
    private notification:NotificationsService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private locationService : LocationService,
    private eventPageResolver: UserSessionResolver

  ) {

    this.eventUserRightsService.getEventUserRights().subscribe(events => {
      this.events = events;
      console.log(events);
    });   

    this.route.data.subscribe(session => {
      this.userSession = session.data;
    });

    this.eventPageResolver.resolve().then(_response => {
      this.userSession = _response;
    });
    
  }




  allow(userEvent: RequestAccessEvent) {
    debugger;
    let _right;
    for (let i = 0; i < this.userSession.usersRight.length; i++) {

      if (this.userSession.usersRight[i].user_ID == userEvent.user_ID)
        _right = this.userSession.usersRight[i];
      console.log(_right);
    }
    if (!_right || _right == null || typeof (_right) == undefined || _right == undefined) {

      let right: UserRight = {
        'location_ID': userEvent.location_ID,
        'user_ID': userEvent.user_ID,
        'permission': true
      };
      this.userRightsService.addUserRight(right);
    }
    else {

      let right_permission: UserRight = {
        'permission': true
      }
      this.userRightsService.updateUserRight(_right.id, right_permission)
    }


    let event: RequestAccessEvent = {
      status: 'resolved'
    }
    this.eventUserRightsService.updatetEventUserRight(userEvent.id, event)

    let title = "Access granted";
    let message= "Hey "+ this.userService.getUserNameById(userEvent.user_ID, this.userSession.users) +", admin granted you access to the office!";
    this.notification.send(userEvent.user_ID, title, message).subscribe(() => {
         
    });

  }
  


  deny(userEvent: RequestAccessEvent) {
    debugger;
    let _right;
    for (let i = 0; i < this.userSession.usersRight.length; i++) {

      if (this.userSession.usersRight[i].user_ID == userEvent.user_ID)
        _right = this.userSession.usersRight[i];
      console.log(_right);
    }
    if (!_right || _right == null || typeof (_right) == undefined || _right == undefined) {

      let right: UserRight = {
        'location_ID': userEvent.location_ID,
        'user_ID': userEvent.user_ID,
        'permission': false
      };
      this.userRightsService.addUserRight(right);
    }
    else {

      let right_permission: UserRight = {
        'permission': false
      }
      this.userRightsService.updateUserRight(_right.id, right_permission)
    }


    let event: RequestAccessEvent = {
      status: 'resolved'
    }
    this.eventUserRightsService.updatetEventUserRight(userEvent.id, event)

    let title = "Access denied";
    let message= "Hey "+ this.userService.getUserNameById(userEvent.user_ID, this.userSession.users) +", admin denied you access to the office!";
    this.notification.send(userEvent.user_ID, title, message).subscribe(() => {
         
    });

  }



  revoke(userEvent: RequestAccessEvent) {
    let event: RequestAccessEvent = {
      status: 'waiting'
    }
    this.eventUserRightsService.updatetEventUserRight(userEvent.id, event);

  }

   // allow(userEvent: RequestAccessEvent) {

  //   let right: UserRight = {
  //     'permission': true,
  //     'location_ID':userEvent.location_ID,
  //     'user_ID': userEvent.user_ID,
  //   }

  //   this.userRightsService.addOrUpdateUserRight(right).then(_response => {

  //     let event: RequestAccessEvent = {
  //       status: 'resolved'
  //     }
  //     this.eventUserRightsService.updatetEventUserRight(userEvent.id, event).then(() => {
  //       let title = "Access granted";
  //       let message= "Hey "+ this.userService.getUserNameById(userEvent.user_ID, this.userSession.users) +", admin granted you access to the office!";
  //       this.notification.send(userEvent.user_ID, title, message).subscribe(() => {
         
  //       });
  //     });
  //   });
  // }
  
}