import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserSessionResolver } from '../resolver/user.session.resolver';
import { IonicModule } from '@ionic/angular';
import { EventPageRoutingModule } from './event-routing.module';
import { EventPage } from './event.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '/event',
        component: EventPage,
        resolve: {
          UserSessionResolver
        }
      }
    ]),
    EventPageRoutingModule
  ],
  declarations: [EventPage],
  providers: [
    UserSessionResolver
  ]
})
export class EventPageModule {}
