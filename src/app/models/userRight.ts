export interface UserRight{
    id?:string,
    location_ID?: string,
    user_ID?: string,
    permission?: boolean
}