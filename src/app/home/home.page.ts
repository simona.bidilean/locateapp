import { Component } from '@angular/core';
import { AuthenticateService } from '../services/authentication.service';
import * as CONSTANTS from '../constants/constants';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-Home',
  templateUrl: './Home.page.html',
  styleUrls: ['./Home.page.scss'],
})
export class HomePage {
  currentUser: User;
  userSession: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticateService,
    private db: Storage,
    private fcm: FCM
  ) {

    this.route.data.subscribe(session => {
      this.userSession = session.data;
      this.currentUser = this.userSession.currentUser;
    });

    this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
      if (this.currentUser.isAdmin) {
        this.fcm.subscribeToTopic(CONSTANTS.REQUEST_ACCESS_TOPIC).then(() => {
          console.log("Admin receives push notifications when access is requested!");
        });  
      } 
       else if(!this.currentUser.isAdmin) {
        
        this.fcm.subscribeToTopic(this.currentUser.uid).then(() => {
          console.log("User receives push notifications when access is granted or denied!");
        });  
      }
      
    });
  }

  logout() {
    this.authService.logoutUser(this.currentUser)
      .then(res => {
        console.log(res);
        this.db.clear().then(() => {
          this.router.navigate(['/']);
        });
      });
  }
}