import { Component, OnInit, Injectable } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { AuthenticateService } from '../services/authentication.service';
import * as firebase from 'firebase/app';
import { UserService } from '../services/user.service';
import { AlertController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { User } from '../models/user';
import * as CONSTANTS from '../constants/constants';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UserSessionResolver } from '../resolver/user.session.resolver';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})

@Injectable()
export class EditPage {
  currentUser: User;
  userSession: any;
  profileForm: any;
  firstName: any;
  lastName: any;
  phone: any;
  address: any;
  function: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public alertController: AlertController,
    private userService: UserService,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private db:Storage,
    private editPageResolver: UserSessionResolver
  ) { 

    this.editPageResolver.resolve().then(_response => {

      this.userSession = _response;
      this.currentUser = this.userSession.currentUser;
      console.log(this.currentUser);
      
    });

    this.route.data.subscribe(session => {
      
      this.userSession = session.data;
      this.currentUser = this.userSession.currentUser;
      console.log(this.currentUser);
    });
  

    
    this.profileForm = this.formBuilder.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      phone: new FormControl(),
      address: new FormControl(),
      function: new FormControl()

    });

  }

 

  onSubmit() {
    debugger;
    this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
      _userSession.currentUser.firstName =this.profileForm.value.firstName;
      _userSession.currentUser.lastName=this.profileForm.value.lastName;
      _userSession.currentUser.phone=this.profileForm.value.phone;
      _userSession.currentUser.address=this.profileForm.value.address;
      _userSession.currentUser.function=this.profileForm.value.function;

      this.db.set(CONSTANTS.USER_SESSION, _userSession).then(() => {

          this.userService.updateUser(this.currentUser.uid, _userSession.currentUser).then(() => {

            this.router.navigate(['/my-account']);
          });
        });
    });

  }


}
