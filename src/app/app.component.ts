import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';
import { Platform, Events, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as CONSTANTS from './constants/constants';
import { User } from './models/user';
import { UserSession } from './models/userSession';
import { AuthenticateService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
    },
    {
      title: 'Map',
      url: '/map',
      icon: 'map',
    },
    {
      title: 'My Account',
      url: '/my-account',
      icon: 'person',
    }
  ];

  constructor(
    private authService: AuthenticateService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public afAuth: AngularFireAuth,
    public events: Events,
    private db: Storage,
    private router: Router,
    private fcm: FCM,
    private toastCtrl: ToastController
  ) {
    this.initializeApp();
  }

  componentDidLoad() {

  }

  initializeApp() {
    this.afAuth.user.subscribe(user => {

      let userInfo:User = user;
      if(userInfo){
        this.router.navigate(["/home"]);
      }else{
        this.router.navigate(["/login"]);
      }
      this.platform.ready().then(() => {
        this.splashScreen.hide();
        this.statusBar.styleDefault();


        this.fcm.getToken().then(userToken => {
          let _newSession = new UserSession();
          _newSession.currentUser = new User();
          _newSession.token = userToken;
          this.db.set(CONSTANTS.USER_SESSION, _newSession).then(() => {
          });
        });

        this.fcm.onNotification().subscribe(data => {
          console.log(data);

          if (data.wasTapped) {
            console.log('Received in background');
          } else {
            console.log('Received in foreground');
            this.presentToast(data);
          }
        });
      });
    });

    // this.authService.authenticationState.subscribe(state => {
    //   if (state) {
    //     this.router.navigate(['/home']);
    //   } else {
    //     this.router.navigate(['/']);
    //   }
      
    // });


  }

  async presentToast(data) {
    const toast = await this.toastCtrl.create({
      message: data.body,
      duration: 4000
    });
    toast.present();
  }


}
