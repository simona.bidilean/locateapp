export interface RequestAccessEvent {
    id?:string,
    location_ID?: string,
    user_ID? : string,
    status?: string
}