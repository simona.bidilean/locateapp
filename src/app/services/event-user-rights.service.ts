import { Injectable } from '@angular/core';
import 'firebase/auth';
import 'firebase/firestore';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/storage';
import { RequestAccessEvent } from '../models/request-access-event';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventUserRightsService {
  private eventUserRights: Observable<RequestAccessEvent[]>;
  private eventUserRightCollection: AngularFirestoreCollection<RequestAccessEvent>;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {

    this.eventUserRightCollection = this.afs.collection<RequestAccessEvent>('eventUserRights');
    this.eventUserRights = this.eventUserRightCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getEventUserRights(): Observable<RequestAccessEvent[]> {
    return this.eventUserRights;
  }

  getEventUserRight(id: string): Observable<RequestAccessEvent> {
    return this.eventUserRightCollection.doc<RequestAccessEvent>(id).valueChanges().pipe(
      take(1),
      map(eventUserRight => {
        eventUserRight.id = id;
        return eventUserRight
      })
    );
  }


  updatetEventUserRight(id: string, event: RequestAccessEvent) {
    return this.eventUserRightCollection.doc(id).update(event);
  }

  addOrUpdateEventUserRight(eventUserRight: RequestAccessEvent): Promise<any> {
    return new Promise<any>((resolve) => {
      this.eventUserRights.subscribe(_currentEvents => {
        let _userEvents = _currentEvents.filter(requestEvent => requestEvent.user_ID === eventUserRight.user_ID);
        if (_userEvents.length > 0) {
          let _userEvent = _userEvents[0];
          this.updatetEventUserRight(_userEvent.id, eventUserRight);
          resolve("Updated");
        } else {
          this.eventUserRightCollection.add(eventUserRight);
          resolve("Added");
        }
      });
    });
  }

  addEventUserRight(eventUserRight: RequestAccessEvent): Promise<DocumentReference> {
    return this.eventUserRightCollection.add(eventUserRight);
  }

  deleteEventUserRight(id: string): Promise<void> {
    return this.eventUserRightCollection.doc(id).delete();
  }
}