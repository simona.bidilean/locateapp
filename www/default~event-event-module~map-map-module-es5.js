(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~event-event-module~map-map-module"],{

/***/ "./src/app/resolver/user.session.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/resolver/user.session.resolver.ts ***!
  \***************************************************/
/*! exports provided: UserSessionResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSessionResolver", function() { return UserSessionResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");











var UserSessionResolver = /** @class */ (function () {
    function UserSessionResolver(db, geolocation, userService, locationService, userRightsService, events) {
        this.db = db;
        this.geolocation = geolocation;
        this.userService = userService;
        this.locationService = locationService;
        this.userRightsService = userRightsService;
        this.events = events;
    }
    UserSessionResolver.prototype.resolve = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_4__["USER_SESSION"]).then(function (_userSession) {
                if (_userSession != null) {
                    if (_userSession.currentUser != null) {
                        if (_userSession.currentUser.id != null) {
                            _this.userService.getUser(_userSession.currentUser.id).subscribe(function (userDetail) {
                                _userSession.currentUser = userDetail;
                                _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(function () {
                                    if (!_userSession.currentUser.isAdmin) {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.geolocation.getCurrentPosition(), _this.locationService.getLocations(), _this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var mylocation = _a[0], locations = _a[1], userRights = _a[2];
                                            return ({ mylocation: mylocation, locations: locations, userRights: userRights });
                                        })).subscribe(function (response) {
                                            _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                                            _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;
                                            _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                                            delete response['mylocation'];
                                            response['users'] = [_userSession.currentUser];
                                            response['usersRight'] = [];
                                            response['usersRight'].push(response['userRights']);
                                            delete response['userRights'];
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    var _permission = response['usersRight'].filter(function (accessRight) { return accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id; })[0].permission;
                                                    if (_permission) {
                                                        _location.hasAccess = _permission;
                                                    }
                                                    else if (!_permission || !response['usersRight']) {
                                                        _location.hasAccess = false;
                                                    }
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.locationService.getLocations(), _this.userService.getUsers(), _this.userRightsService.getUserRights()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var locations = _a[0], users = _a[1], usersRight = _a[2];
                                            return ({ locations: locations, users: users, usersRight: usersRight });
                                        })).subscribe(function (response) {
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    _location.hasAccess = _this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
            });
        });
    };
    UserSessionResolver.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
        { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"] }
    ]; };
    UserSessionResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
            _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
            _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"]])
    ], UserSessionResolver);
    return UserSessionResolver;
}());



/***/ }),

/***/ "./src/app/services/event-user-rights.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/event-user-rights.service.ts ***!
  \*******************************************************/
/*! exports provided: EventUserRightsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventUserRightsService", function() { return EventUserRightsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/storage */ "./node_modules/firebase/storage/dist/index.esm.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");








var EventUserRightsService = /** @class */ (function () {
    function EventUserRightsService(afs, afAuth) {
        this.afs = afs;
        this.afAuth = afAuth;
        this.eventUserRightCollection = this.afs.collection('eventUserRights');
        this.eventUserRights = this.eventUserRightCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (actions) {
            return actions.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ id: id }, data);
            });
        }));
    }
    EventUserRightsService.prototype.getEventUserRights = function () {
        return this.eventUserRights;
    };
    EventUserRightsService.prototype.getEventUserRight = function (id) {
        return this.eventUserRightCollection.doc(id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(function (eventUserRight) {
            eventUserRight.id = id;
            return eventUserRight;
        }));
    };
    EventUserRightsService.prototype.updatetEventUserRight = function (id, event) {
        return this.eventUserRightCollection.doc(id).update(event);
    };
    EventUserRightsService.prototype.addOrUpdateEventUserRight = function (eventUserRight) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.eventUserRights.subscribe(function (_currentEvents) {
                var _userEvents = _currentEvents.filter(function (requestEvent) { return requestEvent.user_ID === eventUserRight.user_ID; });
                if (_userEvents.length > 0) {
                    var _userEvent = _userEvents[0];
                    _this.updatetEventUserRight(_userEvent.id, eventUserRight);
                    resolve("Updated");
                }
                else {
                    _this.eventUserRightCollection.add(eventUserRight);
                    resolve("Added");
                }
            });
        });
    };
    EventUserRightsService.prototype.addEventUserRight = function (eventUserRight) {
        return this.eventUserRightCollection.add(eventUserRight);
    };
    EventUserRightsService.prototype.deleteEventUserRight = function (id) {
        return this.eventUserRightCollection.doc(id).delete();
    };
    EventUserRightsService.ctorParameters = function () { return [
        { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"] },
        { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"] }
    ]; };
    EventUserRightsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_4__["AngularFireAuth"]])
    ], EventUserRightsService);
    return EventUserRightsService;
}());



/***/ }),

/***/ "./src/app/services/notifications.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/notifications.service.ts ***!
  \***************************************************/
/*! exports provided: NotificationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsService", function() { return NotificationsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var NotificationsService = /** @class */ (function () {
    function NotificationsService(http) {
        this.http = http;
    }
    NotificationsService.prototype.send = function (topic, title, message) {
        var body = { "to": "/topics/" + topic, "priority": "high", "notification": { "body": message, "title": title, } };
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json', 'Authorization': 'key=AAAAC_6mwDg:APA91bFOkRg9nCKLTjbM7IWVCa3qpG3f-NWNhH0Fw664wtaLBRfteabx2XczP4BuAafKzjESJAQOy5bD5x_J0hGjDfIDc64EH2H1VInWT9HDxPyEGDBhZwa13IwYlOofJnb2whsEKwBS' })
        };
        return this.http.post("https://fcm.googleapis.com/fcm/send", body, httpOptions);
    };
    NotificationsService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    NotificationsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NotificationsService);
    return NotificationsService;
}());



/***/ })

}]);
//# sourceMappingURL=default~event-event-module~map-map-module-es5.js.map