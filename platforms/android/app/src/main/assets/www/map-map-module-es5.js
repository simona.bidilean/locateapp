(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-map-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/map/map.page.html":
/*!*************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/map.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Map\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n \r\n\r\n  <ion-row>\r\n    <ion-col>\r\n      <div class=\"map-wrapper\">\r\n\r\n        <div #map id=\"map\"></div>\r\n      </div>\r\n     \r\n   \r\n\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  \r\n\r\n<ion-row>\r\n  <ion-col text-center>\r\n    <ion-button (click)=\"requestAccess('You are about to request access. Continue?')\" [disabled]=\"disableBtn\" *ngIf=\"!currentUser.isAdmin\" color=\"primary\" expand=\"block\" size=\"large\">REQUEST ACCESS</ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n\r\n \r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-row>\r\n    <ion-col text-center>\r\n  <ion-button (click)=\"logout()\" color=\"dark\">\r\n    Log out\r\n  </ion-button>\r\n</ion-col>\r\n</ion-row>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/map/map-routing.module.ts":
/*!*******************************************!*\
  !*** ./src/app/map/map-routing.module.ts ***!
  \*******************************************/
/*! exports provided: MapPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageRoutingModule", function() { return MapPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map.page */ "./src/app/map/map.page.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");





var routes = [
    {
        path: '',
        component: _map_page__WEBPACK_IMPORTED_MODULE_3__["MapPage"],
        resolve: {
            data: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
        }
    }
];
var MapPageRoutingModule = /** @class */ (function () {
    function MapPageRoutingModule() {
    }
    MapPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MapPageRoutingModule);
    return MapPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/map/map.module.ts":
/*!***********************************!*\
  !*** ./src/app/map/map.module.ts ***!
  \***********************************/
/*! exports provided: MapPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map.page */ "./src/app/map/map.page.ts");
/* harmony import */ var _map_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./map-routing.module */ "./src/app/map/map-routing.module.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");









var MapPageModule = /** @class */ (function () {
    function MapPageModule() {
    }
    MapPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '/map',
                        component: _map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"],
                        resolve: {
                            UserSessionResolver: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__["UserSessionResolver"]
                        }
                    }
                ]),
                _map_routing_module__WEBPACK_IMPORTED_MODULE_7__["MapPageRoutingModule"]
            ],
            declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]],
            providers: [
                _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__["UserSessionResolver"]
            ]
        })
    ], MapPageModule);
    return MapPageModule;
}());



/***/ }),

/***/ "./src/app/map/map.page.scss":
/*!***********************************!*\
  !*** ./src/app/map/map.page.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map {\n  width: 100%;\n  height: 70vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL0M6XFxVc2Vyc1xcc2ltb25hLmJpZGlsZWFuXFxEb2N1bWVudHNcXGxvY2F0ZUFwcFxcbG9jYXRlYXBwL3NyY1xcYXBwXFxtYXBcXG1hcC5wYWdlLnNjc3MiLCJzcmMvYXBwL21hcC9tYXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL21hcC9tYXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIFxyXG4jbWFwIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcwdmg7XHJcbn1cclxuIFxyXG5cclxuIiwiI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDcwdmg7XG59Il19 */"

/***/ }),

/***/ "./src/app/map/map.page.ts":
/*!*********************************!*\
  !*** ./src/app/map/map.page.ts ***!
  \*********************************/
/*! exports provided: MapPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPage", function() { return MapPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_map_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/map.service */ "./src/app/services/map.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/event-user-rights.service */ "./src/app/services/event-user-rights.service.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../services/notifications.service */ "./src/app/services/notifications.service.ts");














var MapPage = /** @class */ (function () {
    function MapPage(platform, router, route, alertController, db, authService, userService, mapService, mapPageResolver, requestAccessService, notification) {
        var _this = this;
        this.platform = platform;
        this.router = router;
        this.route = route;
        this.alertController = alertController;
        this.db = db;
        this.authService = authService;
        this.userService = userService;
        this.mapService = mapService;
        this.mapPageResolver = mapPageResolver;
        this.requestAccessService = requestAccessService;
        this.notification = notification;
        this.markers = [];
        this.accessGranted = false;
        this.authorisedUserFullName = [];
        this.SCHEDULER_INTERVAL_IN_MILISEC = 8000;
        this.authorizedUsers = [];
        this.disableBtn = false;
        this.route.data.subscribe(function (session) {
            _this.userSession = session.data;
            _this.currentUser = _this.userSession.currentUser;
            if (_this.userSession.locations[0].hasAccess) {
                _this.disableBtn = true;
            }
            else if (!(_this.mapService.isUserAround(_this.currentUser.Location, _this.userSession.locations[0].coords))) {
                _this.disableBtn = true;
            }
            else {
                _this.disableBtn = false;
            }
        });
        this.mapPageResolver.resolve().then(function (_response) {
            _this.userSession = _response;
            _this.currentUser = _this.userSession.currentUser;
            if (_this.userSession.locations[0].hasAccess) {
                _this.disableBtn = true;
            }
            else if (!(_this.mapService.isUserAround(_this.currentUser.Location, _this.userSession.locations[0].coords))) {
                _this.disableBtn = true;
            }
            else {
                _this.disableBtn = false;
            }
        });
        this.requestAccessService.getEventUserRights().subscribe(function (events) {
            _this.events = events;
        });
    }
    MapPage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.platform.ready()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.init()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.bind()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MapPage.prototype.ionWillAppear = function () {
        debugger;
        if (this.scheduler) {
            window.clearInterval(this.scheduler);
        }
    };
    MapPage.prototype.ionViewDidLeave = function () {
        window.clearInterval(this.scheduler);
        this.authorisedUserFullName = [];
        if (this.requestAccessPopup) {
            this.requestAccessPopup.dismiss();
        }
    };
    MapPage.prototype.init = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var mapOptions;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                mapOptions = {
                    center: new google.maps.LatLng(this.userSession.locations[0].coords.latitude, this.userSession.locations[0].coords.longitude),
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                if (this.map == null) {
                    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                }
                this.accessGranted = this.userSession.locations.every(function (_location) { return _location.hasAccess === true; }) || this.currentUser.isAdmin;
                return [2 /*return*/];
            });
        });
    };
    MapPage.prototype.update = function () {
        var _this = this;
        this.scheduler = setInterval(function () {
            clearInterval(_this.scheduler);
            _this.deleteMarkers();
            _this.mapPageResolver.resolve().then(function (_response) {
                debugger;
                _this.userSession = _response;
                _this.currentUser = _this.userSession.currentUser;
                if (_this.userSession.locations[0].hasAccess) {
                    _this.disableBtn = true;
                }
                else if (!(_this.mapService.isUserAround(_this.currentUser.Location, _this.userSession.locations[0].coords))) {
                    _this.disableBtn = true;
                }
                else {
                    _this.disableBtn = false;
                }
                _this.bind();
            });
        }, this.SCHEDULER_INTERVAL_IN_MILISEC);
    };
    MapPage.prototype.bind = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.userSession.users.forEach(function (_user, uIndex) {
                    if (!_this.currentUser.isAdmin) {
                        //
                        _this.markers.push(_this.mapService.createMarker(_this.map, _user.Location, true, true, true, "You current location"));
                    }
                    else {
                        if (_user.id != _this.currentUser.id) {
                            var _fullName = _user.firstName + " " + _user.lastName;
                            if (_this.mapService.isUserAround(_user.Location, _this.userSession.locations[0].coords) && _user.isOnline) {
                                debugger;
                                //
                                _this.markers.push(_this.mapService.createMarker(_this.map, _user.Location, true, true, true, _fullName));
                            }
                        }
                    }
                    if (uIndex === _this.userSession.users.length - 1) {
                        _this.userSession.locations.forEach(function (_location, lIndex) {
                            _this.authorizedUsers = [];
                            _this.authorisedUserFullName = [];
                            _this.authorizedUsers = _this.userSession.usersRight.filter(function (right) { return right.permission == true; });
                            _this.authorizedUsers.forEach(function (_authorizedUser, auIndex) {
                                if (_this.authorisedUserFullName.length < _this.authorizedUsers.length) {
                                    _this.authorisedUserFullName.push(_this.userService.getUserAccessFullName(_authorizedUser.user_ID, _location.id, _this.userSession.users, _this.userSession.locations));
                                }
                                if (auIndex === _this.authorizedUsers.length - 1) {
                                    _this.officeMarkerDescription = null;
                                    if (_this.authorisedUserFullName.length > 0) {
                                        _this.officeMarkerDescription = _this.authorisedUserFullName.join();
                                    }
                                    //
                                    _this.markers.push(_this.mapService.createMarker(_this.map, _location.coords, false, _this.currentUser.isAdmin, _location.hasAccess, _this.officeMarkerDescription));
                                }
                            });
                            if (!_this.currentUser.isAdmin) {
                                _this.markers.push(_this.mapService.createMarker(_this.map, _location.coords, false, _this.currentUser.isAdmin, _location.hasAccess));
                            }
                            _this.setMapOnAll(_this.map);
                            _this.update();
                        });
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    MapPage.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    MapPage.prototype.clearMarkers = function () {
        this.setMapOnAll(null);
    };
    MapPage.prototype.deleteMarkers = function () {
        this.clearMarkers();
        this.markers = [];
    };
    MapPage.prototype.requestAccess = function (headerText) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        clearInterval(this.scheduler);
                        _a = this;
                        return [4 /*yield*/, this.alertController.create({
                                header: headerText,
                                buttons: [
                                    {
                                        text: 'Cancel',
                                        role: 'cancel',
                                        cssClass: 'secondary',
                                        handler: function () {
                                            _this.update();
                                        }
                                    }, {
                                        text: 'Yes',
                                        handler: function () {
                                            debugger;
                                            var _event;
                                            var event1 = {
                                                'status': 'waiting',
                                                'location_ID': _this.userSession.locations[0].id,
                                                'user_ID': _this.currentUser.uid,
                                            };
                                            var event2 = {
                                                'status': 'waiting'
                                            };
                                            for (var i = 0; i < _this.events.length; i++) {
                                                if (_this.events[i].user_ID == _this.currentUser.uid) {
                                                    _event = _this.events[i];
                                                }
                                            }
                                            if (_event) {
                                                _this.requestAccessService.updatetEventUserRight(_event.id, event2);
                                            }
                                            else {
                                                _this.requestAccessService.addEventUserRight(event1);
                                            }
                                            var title = _this.currentUser.firstName + " " + _this.currentUser.lastName;
                                            var message = title + " requests access to the office";
                                            _this.notification.send(_constants_constants__WEBPACK_IMPORTED_MODULE_3__["REQUEST_ACCESS_TOPIC"], title, message).subscribe(function () {
                                                _this.update();
                                            });
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        _a.requestAccessPopup = _b.sent();
                        return [4 /*yield*/, this.requestAccessPopup.present()];
                    case 2:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MapPage.prototype.logout = function () {
        var _this = this;
        this.authService.logoutUser(this.currentUser).then(function (res) {
            console.log(res);
            _this.db.clear().then(function () {
                _this.router.navigate(['/']);
            });
        });
    };
    MapPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__["AuthenticateService"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"] },
        { type: _services_map_service__WEBPACK_IMPORTED_MODULE_9__["MapService"] },
        { type: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__["UserSessionResolver"] },
        { type: _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__["EventUserRightsService"] },
        { type: _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__["NotificationsService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-Map',
            template: __webpack_require__(/*! raw-loader!./map.page.html */ "./node_modules/raw-loader/index.js!./src/app/map/map.page.html"),
            styles: [__webpack_require__(/*! ./map.page.scss */ "./src/app/map/map.page.scss")]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__["AuthenticateService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"],
            _services_map_service__WEBPACK_IMPORTED_MODULE_9__["MapService"],
            _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__["UserSessionResolver"],
            _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__["EventUserRightsService"],
            _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__["NotificationsService"]])
    ], MapPage);
    return MapPage;
}());



/***/ })

}]);
//# sourceMappingURL=map-map-module-es5.js.map