import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController } from '@ionic/angular';
import { AuthenticateService } from '../services/authentication.service';
import * as CONSTANTS from '../constants/constants';
import { FCM } from '@ionic-native/fcm/ngx';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UserSession } from '../models/userSession';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  validations_form: FormGroup;
  errorMessage: string = '';
  disableLoginBtn: boolean = true;
  loading: any;
  constructor(
    private userService: UserService,
    private router: Router,
    private db: Storage,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public afAuth: AngularFireAuth,
    private authService: AuthenticateService,
    private fcm: FCM
  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await this.loading.present();

    const { role, data } = await this.loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  ionViewDidLoad() {
    this.disableLoginBtn = !this.validations_form.valid;
  }

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };


  isLoginBtnEnabled() {
    return !this.validations_form.valid;
  }

  loginUser(value) {
    this.presentLoading();
    this.disableLoginBtn = true;
    this.authService.loginUser(value).then(_loginResponse => {
      this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
        _userSession.currentUser.id = _loginResponse.user.uid;
        _userSession.currentUser.isOnline = true;
        this.fcm.subscribeToTopic(_userSession.currentUser.id).then(() => {
          this.db.set(CONSTANTS.USER_SESSION, _userSession).then(() => {

            this.userService.updateUser(_userSession.currentUser.id, _userSession.currentUser).then(() => {


              this.loading.dismiss();
              this.router.navigate(["/home"]);
            });

          });

        });
      });
    }).catch(err => {
      console.log(`login failed ${err}`);
      this.errorMessage = err.message;
      this.loading.dismiss();
    });
  }

  goToRegisterPage() {
    this.router.navigate(['/register']);
  }
}