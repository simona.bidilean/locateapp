import { Component, OnInit, ViewChild, ElementRef, Injectable } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import * as firebase from 'firebase/app';
import * as CONSTANTS from '../constants/constants';
import 'firebase/auth';
import 'firebase/firestore';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { MapService } from '../services/map.service';
import { User } from '../models/user';
import { OfficeLocation } from '../models/location';
import { AuthenticateService } from '../services/authentication.service';
import { GoogleMaps, GoogleMap } from '@ionic-native/google-maps';
import { RequestAccessEvent } from '../models/request-access-event';
import { EventUserRightsService } from '../services/event-user-rights.service';
import { UserSessionResolver } from '../resolver/user.session.resolver';
import { NotificationsService } from '../services/notifications.service';

declare var google;

@Component({
  selector: 'app-Map',
  templateUrl: 'map.page.html',
  styleUrls: ['map.page.scss'],
})

@Injectable()
export class MapPage implements OnInit {

  @ViewChild('map', { static: false })
  mapElement: ElementRef;
  map: GoogleMap;
  currentUser: User;
  userSession: any;
  locations: OfficeLocation[];
  markers: any[] = [];
  requestAccessPopup: HTMLIonAlertElement;
  accessGranted: boolean = false;
  scheduler: any;
  authorisedUserFullName: string[] = [];
  SCHEDULER_INTERVAL_IN_MILISEC = 8000;
  officeMarkerDescription: string;
  authorizedUsers = [];
  disableBtn: boolean = false;
  events: RequestAccessEvent[];


  constructor(
    private platform: Platform,
    private router: Router,
    private route: ActivatedRoute,
    public alertController: AlertController,
    private db: Storage,
    private authService: AuthenticateService,
    private userService: UserService,
    private mapService: MapService,
    private mapPageResolver: UserSessionResolver,
    private requestAccessService: EventUserRightsService,
    private notification: NotificationsService
  ) {
    this.route.data.subscribe(session => {
      this.userSession = session.data;
      this.currentUser = this.userSession.currentUser;
      if (this.userSession.locations[0].hasAccess) {
        this.disableBtn = true;
      } else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
        this.disableBtn = true;
      } else {
        this.disableBtn = false;
      }

    });

    this.mapPageResolver.resolve().then(_response => {
      this.userSession = _response;
      this.currentUser = this.userSession.currentUser;
      if (this.userSession.locations[0].hasAccess) {
        this.disableBtn = true;
      } else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
        this.disableBtn = true;
      } else {
        this.disableBtn = false;
      }

    });

    this.requestAccessService.getEventUserRights().subscribe(events => {
      this.events =events
    });

  }

  async ngOnInit() {
    await this.platform.ready();
    await this.init();
    await this.bind();
  }



  ionWillAppear() {
    debugger;
    if (this.scheduler) {
      window.clearInterval(this.scheduler);
    }
  }

  ionViewDidLeave() {
    window.clearInterval(this.scheduler);
    this.authorisedUserFullName = [];
    if (this.requestAccessPopup) {
      this.requestAccessPopup.dismiss();
    }
  }

  async init() {

    let mapOptions = {
      center: new google.maps.LatLng(this.userSession.locations[0].coords.latitude, this.userSession.locations[0].coords.longitude),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    if (this.map == null) {
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    }
    this.accessGranted = this.userSession.locations.every(_location => _location.hasAccess === true) || this.currentUser.isAdmin;
  }

  update() {
    this.scheduler = setInterval(() => {
      clearInterval(this.scheduler);
      this.deleteMarkers();
      this.mapPageResolver.resolve().then(_response => {
        debugger;
        this.userSession = _response;
        this.currentUser = this.userSession.currentUser;

        if (this.userSession.locations[0].hasAccess) {
          this.disableBtn = true;
        } else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
          this.disableBtn = true;
        } else {
          this.disableBtn = false;
        }

        this.bind();
      });
    }, this.SCHEDULER_INTERVAL_IN_MILISEC);
  }

  async bind() {
    this.userSession.users.forEach((_user, uIndex) => {
      if (!this.currentUser.isAdmin) {
        //
        this.markers.push(this.mapService.createMarker(this.map, _user.Location, true, true, true, "You current location"));
      } else {
        if (_user.id != this.currentUser.id) {
          let _fullName = _user.firstName + " " + _user.lastName;
          if (this.mapService.isUserAround(_user.Location, this.userSession.locations[0].coords) && _user.isOnline) {
            debugger;
            //
            this.markers.push(this.mapService.createMarker(this.map, _user.Location, true, true, true, _fullName));
          }

        }
      }

      if (uIndex === this.userSession.users.length - 1) {
        this.userSession.locations.forEach((_location, lIndex) => {
          this.authorizedUsers = [];
          this.authorisedUserFullName = [];
          this.authorizedUsers = this.userSession.usersRight.filter((right) => right.permission == true);

          this.authorizedUsers.forEach((_authorizedUser, auIndex) => {
            if (this.authorisedUserFullName.length < this.authorizedUsers.length) {
              this.authorisedUserFullName.push(this.userService.getUserAccessFullName(_authorizedUser.user_ID, _location.id, this.userSession.users, this.userSession.locations));
            }

            if (auIndex === this.authorizedUsers.length - 1) {
              this.officeMarkerDescription = null;
              if (this.authorisedUserFullName.length > 0) {
                this.officeMarkerDescription = this.authorisedUserFullName.join();
              }
              //
              this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, this.currentUser.isAdmin, _location.hasAccess, this.officeMarkerDescription));
            }
          });

          if (!this.currentUser.isAdmin) {
            this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, this.currentUser.isAdmin, _location.hasAccess));
          }


          this.setMapOnAll(this.map);


          this.update();
        });
      }
    });
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }



  async requestAccess(headerText) {
    clearInterval(this.scheduler);
    this.requestAccessPopup = await this.alertController.create({
      header: headerText,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.update();
          }
        }, {
          text: 'Yes',
          handler: () => {
            debugger;

            let _event;
            let event1: RequestAccessEvent = {
              'status': 'waiting',
              'location_ID': this.userSession.locations[0].id,
              'user_ID': this.currentUser.uid,
            }
            let event2: RequestAccessEvent={
              'status': 'waiting'
            }
            
              for (let i = 0; i < this.events.length; i++) {
                if (this.events[i].user_ID == this.currentUser.uid) {
                  _event = this.events[i];
                }
              }
              if (_event) {
                this.requestAccessService.updatetEventUserRight(_event.id, event2)
              } else {
                this.requestAccessService.addEventUserRight(event1);
              }
           

            let title = this.currentUser.firstName + " " + this.currentUser.lastName;
            let message = title + " requests access to the office";
            this.notification.send(CONSTANTS.REQUEST_ACCESS_TOPIC, title, message).subscribe(() => {
              this.update();
            });
            

          }
        }
      ]
    });
    await this.requestAccessPopup.present();
  }



  logout() {
    this.authService.logoutUser(this.currentUser).then(res => {
      console.log(res);
      this.db.clear().then(() => {
        this.router.navigate(['/']);
      });
    });
  }
}
