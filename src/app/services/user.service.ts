import { Injectable } from '@angular/core';
import 'firebase/auth';
import 'firebase/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/storage';
import { User } from '../models/user';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OfficeLocation } from '../models/location';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: Observable<User[]>;
  private usersCollection: AngularFirestoreCollection<User>;
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
  ) {


    this.usersCollection = this.afs.collection<User>('users');
    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getUsers(): Observable<User[]> {
    return this.users;
  }

  getUser(id: string): Observable<User> {
    return this.usersCollection.doc<User>(id).valueChanges().pipe(
      take(1),
      map(user => {
        user.id = id;
        return user;
      })
    );
  }

  getUserAccessFullName(userId: string, locationId: string, users: User[], locations: OfficeLocation[]): string {
    for (var i = 0; i < users.length; i++) {
      for (var j = 0; j < locations.length; j++) {
        if (users[i].id == userId && locations[j].id == locationId) {
          return users[i].firstName + " " + users[i].lastName;
        }
      }
    }
  }
  
  getUserNameById(userId:string, users: User[]){
    for(var i = 0; i< users.length; i++){
      if(users[i].uid == userId){
        return users[i].firstName;
      }
    }
  }

  updateUser(uid: string, userInfo: User) {
    return this.usersCollection.doc(uid).update(userInfo);
  }

  deleteUser(id: string): Promise<void> {
   
    return firebase.auth().currentUser.delete().then(() => {
         this.usersCollection.doc(id).delete();
    });
    
  }
}