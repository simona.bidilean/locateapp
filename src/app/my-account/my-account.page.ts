import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { AuthenticateService } from '../services/authentication.service';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { UserService } from '../services/user.service';
import { AlertController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { User } from '../models/user';
import * as CONSTANTS from '../constants/constants';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.page.html',
  styleUrls: ['./my-account.page.scss'],
})
export class MyAccountPage {
  currentUser: User;
  userSession: any;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public alertController: AlertController,
    private userService: UserService,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private db:Storage
  ) {

    this.route.data.subscribe(session => {
      this.userSession = session.data;
      this.currentUser = this.userSession.currentUser;
    });



  }



  async deleteAccount() {
    const alert = await this.alertController.create({
      header: 'Are you sure of this? ',
      message: 'Your account will be deleted and you will be logged out.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Okay');
            this.userService.deleteUser(this.currentUser.id).then(resepnose => {
              console.log(resepnose);
              this.logout();
            }
            );
          }
        }
      ]
    });

    await alert.present();
  }


  logout() {
    this.authService.logoutUser(this.currentUser)
      .then(res => {
        console.log(res);
        this.db.clear().then(() => {
          this.router.navigate(['/']);
        });
      });
  }

}
