(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-map-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/map/map.page.html":
/*!*************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/map/map.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n      Map\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n \r\n\r\n  <ion-row>\r\n    <ion-col>\r\n      <div class=\"map-wrapper\">\r\n\r\n        <div #map id=\"map\"></div>\r\n      </div>\r\n     \r\n   \r\n\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  \r\n\r\n<ion-row>\r\n  <ion-col text-center>\r\n    <ion-button (click)=\"requestAccess('You are about to request access. Continue?')\" [disabled]=\"disableBtn\" *ngIf=\"!currentUser.isAdmin\" color=\"primary\" expand=\"block\" size=\"large\">REQUEST ACCESS</ion-button>\r\n  </ion-col>\r\n</ion-row>\r\n\r\n\r\n\r\n \r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-row>\r\n    <ion-col text-center>\r\n  <ion-button (click)=\"logout()\" color=\"dark\">\r\n    Log out\r\n  </ion-button>\r\n</ion-col>\r\n</ion-row>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/map/map-routing.module.ts":
/*!*******************************************!*\
  !*** ./src/app/map/map-routing.module.ts ***!
  \*******************************************/
/*! exports provided: MapPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageRoutingModule", function() { return MapPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _map_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./map.page */ "./src/app/map/map.page.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");





const routes = [
    {
        path: '',
        component: _map_page__WEBPACK_IMPORTED_MODULE_3__["MapPage"],
        resolve: {
            data: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
        }
    }
];
let MapPageRoutingModule = class MapPageRoutingModule {
};
MapPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MapPageRoutingModule);



/***/ }),

/***/ "./src/app/map/map.module.ts":
/*!***********************************!*\
  !*** ./src/app/map/map.module.ts ***!
  \***********************************/
/*! exports provided: MapPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./map.page */ "./src/app/map/map.page.ts");
/* harmony import */ var _map_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./map-routing.module */ "./src/app/map/map-routing.module.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");









let MapPageModule = class MapPageModule {
};
MapPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '/map',
                    component: _map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"],
                    resolve: {
                        UserSessionResolver: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__["UserSessionResolver"]
                    }
                }
            ]),
            _map_routing_module__WEBPACK_IMPORTED_MODULE_7__["MapPageRoutingModule"]
        ],
        declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]],
        providers: [
            _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_8__["UserSessionResolver"]
        ]
    })
], MapPageModule);



/***/ }),

/***/ "./src/app/map/map.page.scss":
/*!***********************************!*\
  !*** ./src/app/map/map.page.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map {\n  width: 100%;\n  height: 70vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL0M6XFxVc2Vyc1xcc2ltb25hLmJpZGlsZWFuXFxEb2N1bWVudHNcXGxvY2F0ZUFwcFxcbG9jYXRlYXBwL3NyY1xcYXBwXFxtYXBcXG1hcC5wYWdlLnNjc3MiLCJzcmMvYXBwL21hcC9tYXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL21hcC9tYXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIFxyXG4jbWFwIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcwdmg7XHJcbn1cclxuIFxyXG5cclxuIiwiI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDcwdmg7XG59Il19 */"

/***/ }),

/***/ "./src/app/map/map.page.ts":
/*!*********************************!*\
  !*** ./src/app/map/map.page.ts ***!
  \*********************************/
/*! exports provided: MapPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPage", function() { return MapPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_map_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/map.service */ "./src/app/services/map.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/event-user-rights.service */ "./src/app/services/event-user-rights.service.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../services/notifications.service */ "./src/app/services/notifications.service.ts");














let MapPage = class MapPage {
    constructor(platform, router, route, alertController, db, authService, userService, mapService, mapPageResolver, requestAccessService, notification) {
        this.platform = platform;
        this.router = router;
        this.route = route;
        this.alertController = alertController;
        this.db = db;
        this.authService = authService;
        this.userService = userService;
        this.mapService = mapService;
        this.mapPageResolver = mapPageResolver;
        this.requestAccessService = requestAccessService;
        this.notification = notification;
        this.markers = [];
        this.accessGranted = false;
        this.authorisedUserFullName = [];
        this.SCHEDULER_INTERVAL_IN_MILISEC = 8000;
        this.authorizedUsers = [];
        this.disableBtn = false;
        this.route.data.subscribe(session => {
            this.userSession = session.data;
            this.currentUser = this.userSession.currentUser;
            if (this.userSession.locations[0].hasAccess) {
                this.disableBtn = true;
            }
            else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
                this.disableBtn = true;
            }
            else {
                this.disableBtn = false;
            }
        });
        this.mapPageResolver.resolve().then(_response => {
            this.userSession = _response;
            this.currentUser = this.userSession.currentUser;
            if (this.userSession.locations[0].hasAccess) {
                this.disableBtn = true;
            }
            else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
                this.disableBtn = true;
            }
            else {
                this.disableBtn = false;
            }
        });
        this.requestAccessService.getEventUserRights().subscribe(events => {
            this.events = events;
        });
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.platform.ready();
            yield this.init();
            yield this.bind();
        });
    }
    ionWillAppear() {
        debugger;
        if (this.scheduler) {
            window.clearInterval(this.scheduler);
        }
    }
    ionViewDidLeave() {
        window.clearInterval(this.scheduler);
        this.authorisedUserFullName = [];
        if (this.requestAccessPopup) {
            this.requestAccessPopup.dismiss();
        }
    }
    init() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let mapOptions = {
                center: new google.maps.LatLng(this.userSession.locations[0].coords.latitude, this.userSession.locations[0].coords.longitude),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            if (this.map == null) {
                this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            }
            this.accessGranted = this.userSession.locations.every(_location => _location.hasAccess === true) || this.currentUser.isAdmin;
        });
    }
    update() {
        this.scheduler = setInterval(() => {
            clearInterval(this.scheduler);
            this.deleteMarkers();
            this.mapPageResolver.resolve().then(_response => {
                debugger;
                this.userSession = _response;
                this.currentUser = this.userSession.currentUser;
                if (this.userSession.locations[0].hasAccess) {
                    this.disableBtn = true;
                }
                else if (!(this.mapService.isUserAround(this.currentUser.Location, this.userSession.locations[0].coords))) {
                    this.disableBtn = true;
                }
                else {
                    this.disableBtn = false;
                }
                this.bind();
            });
        }, this.SCHEDULER_INTERVAL_IN_MILISEC);
    }
    bind() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.userSession.users.forEach((_user, uIndex) => {
                if (!this.currentUser.isAdmin) {
                    //
                    this.markers.push(this.mapService.createMarker(this.map, _user.Location, true, true, true, "You current location"));
                }
                else {
                    if (_user.id != this.currentUser.id) {
                        let _fullName = _user.firstName + " " + _user.lastName;
                        if (this.mapService.isUserAround(_user.Location, this.userSession.locations[0].coords) && _user.isOnline) {
                            debugger;
                            //
                            this.markers.push(this.mapService.createMarker(this.map, _user.Location, true, true, true, _fullName));
                        }
                    }
                }
                if (uIndex === this.userSession.users.length - 1) {
                    this.userSession.locations.forEach((_location, lIndex) => {
                        this.authorizedUsers = [];
                        this.authorisedUserFullName = [];
                        this.authorizedUsers = this.userSession.usersRight.filter((right) => right.permission == true);
                        this.authorizedUsers.forEach((_authorizedUser, auIndex) => {
                            if (this.authorisedUserFullName.length < this.authorizedUsers.length) {
                                this.authorisedUserFullName.push(this.userService.getUserAccessFullName(_authorizedUser.user_ID, _location.id, this.userSession.users, this.userSession.locations));
                            }
                            if (auIndex === this.authorizedUsers.length - 1) {
                                this.officeMarkerDescription = null;
                                if (this.authorisedUserFullName.length > 0) {
                                    this.officeMarkerDescription = this.authorisedUserFullName.join();
                                }
                                //
                                this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, this.currentUser.isAdmin, _location.hasAccess, this.officeMarkerDescription));
                            }
                        });
                        if (!this.currentUser.isAdmin) {
                            this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, this.currentUser.isAdmin, _location.hasAccess));
                        }
                        this.setMapOnAll(this.map);
                        this.update();
                    });
                }
            });
        });
    }
    setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    clearMarkers() {
        this.setMapOnAll(null);
    }
    deleteMarkers() {
        this.clearMarkers();
        this.markers = [];
    }
    requestAccess(headerText) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            clearInterval(this.scheduler);
            this.requestAccessPopup = yield this.alertController.create({
                header: headerText,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            this.update();
                        }
                    }, {
                        text: 'Yes',
                        handler: () => {
                            debugger;
                            let _event;
                            let event1 = {
                                'status': 'waiting',
                                'location_ID': this.userSession.locations[0].id,
                                'user_ID': this.currentUser.uid,
                            };
                            let event2 = {
                                'status': 'waiting'
                            };
                            for (let i = 0; i < this.events.length; i++) {
                                if (this.events[i].user_ID == this.currentUser.uid) {
                                    _event = this.events[i];
                                }
                            }
                            if (_event) {
                                this.requestAccessService.updatetEventUserRight(_event.id, event2);
                            }
                            else {
                                this.requestAccessService.addEventUserRight(event1);
                            }
                            let title = this.currentUser.firstName + " " + this.currentUser.lastName;
                            let message = title + " requests access to the office";
                            this.notification.send(_constants_constants__WEBPACK_IMPORTED_MODULE_3__["REQUEST_ACCESS_TOPIC"], title, message).subscribe(() => {
                                this.update();
                            });
                        }
                    }
                ]
            });
            yield this.requestAccessPopup.present();
        });
    }
    logout() {
        this.authService.logoutUser(this.currentUser).then(res => {
            console.log(res);
            this.db.clear().then(() => {
                this.router.navigate(['/']);
            });
        });
    }
};
MapPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
    { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__["AuthenticateService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"] },
    { type: _services_map_service__WEBPACK_IMPORTED_MODULE_9__["MapService"] },
    { type: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__["UserSessionResolver"] },
    { type: _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__["EventUserRightsService"] },
    { type: _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__["NotificationsService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], MapPage.prototype, "mapElement", void 0);
MapPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-Map',
        template: __webpack_require__(/*! raw-loader!./map.page.html */ "./node_modules/raw-loader/index.js!./src/app/map/map.page.html"),
        styles: [__webpack_require__(/*! ./map.page.scss */ "./src/app/map/map.page.scss")]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
        _services_authentication_service__WEBPACK_IMPORTED_MODULE_10__["AuthenticateService"],
        _services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"],
        _services_map_service__WEBPACK_IMPORTED_MODULE_9__["MapService"],
        _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_12__["UserSessionResolver"],
        _services_event_user_rights_service__WEBPACK_IMPORTED_MODULE_11__["EventUserRightsService"],
        _services_notifications_service__WEBPACK_IMPORTED_MODULE_13__["NotificationsService"]])
], MapPage);



/***/ })

}]);
//# sourceMappingURL=map-map-module-es2015.js.map