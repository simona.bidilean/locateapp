// import { Component, ViewChild, ElementRef, Injectable } from '@angular/core';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
// import * as firebase from 'firebase/app';
// import 'firebase/auth';
// import 'firebase/firestore';
// import { ActivatedRoute, Router } from '@angular/router';
// import { UserService } from '../services/user.service';
// import { MapService } from '../services/map.service';
// import { LocationService } from '../services/location.service';
// import { User } from '../models/user';
// import { OfficeLocation } from '../models/location';
// import { Events } from '@ionic/angular';
// import { AuthenticateService } from '../services/authentication.service';
// import { Storage } from '@ionic/storage';
// import * as CONSTANTS from '../constants/constants';
// import { GoogleMap } from '@ionic-native/google-maps';
// import { AlertController } from '@ionic/angular';
// import { UserRightsService } from '../services/user-rights.service';
// import UtilityService from '../helper/utility.service';


// declare var google;

// @Component({
//   selector: 'app-Map',
//   templateUrl: 'map.page.html',
//   styleUrls: ['map.page.scss'],
// })

// @Injectable()
// export class MapPage {

//   @ViewChild('map', { static: false })
//   mapElement: ElementRef;
//   map: GoogleMap;
//   currentUser: User;
//   userSession: any;
//   locations: OfficeLocation[];
//   markers: any[];
//   alert: HTMLIonAlertElement;
//   accessGranted: boolean = false;
//   scheduler: any;
//   authorisedUserFullName: string[] = [];
//   private watchID: any;
//   SCHEDULER_INTERVAL_IN_MILISEC = 10000;
//   stopLocationUpdate: boolean = false;
//   locationSubscription: any;

//   constructor(
//     private router: Router,
//     private route: ActivatedRoute,
//     public events: Events,
//     private db: Storage,
//     private geolocation: Geolocation,
//     public alertController: AlertController,
//     private authService: AuthenticateService,
//     private userRightsService: UserRightsService,
//     private locationService: LocationService,
//     private userService: UserService,
//     private mapService: MapService,
//   ) {
//     this.route.data.subscribe(session => {
//       this.userSession = session.data;
//       this.currentUser = this.userSession.currentUser;
//     });
//   }

//   ionViewDidEnter() {
//     this.init();
//   }

//   ionViewWillLeave() {
//     // this.alertController = null;
//   }

//   ionWillAppear() {
//     if(this.scheduler) {
//       window.clearInterval(this.scheduler);
//     }
//   }

//   ionViewDidLeave() {
//     window.clearInterval(this.scheduler);
//     this.authorisedUserFullName = [];
//     this.alertController = null;
//     // this.stopWatchingForLocation();
//   }

//   init() {

//     let mapOptions = {
//       center: new google.maps.LatLng(this.userSession.locations[0].coords.latitude, this.userSession.locations[0].coords.longitude),
//       zoom: 15,
//       mapTypeId: google.maps.MapTypeId.ROADMAP
//     };

//     if (this.map == null) {
//       this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
//     }

//     this.userSession.users.forEach((_user, uIndex) => {
//       if (!_user.isAdmin) {
//         let _name = _user.firstName + ' ' + _user.lastName;
//         this.markers.push(this.mapService.createMarker(this.map, _user.Location, true, true, true, _name));
//       }

//       if (uIndex === this.userSession.users.length - 1) {
//         this.userSession.locations.forEach((_location, lIndex) => {
//           let _authorizedUsers = this.userSession.usersRight.filter((right) => right.permission == true && right.location_ID == _location.id && right.user_ID == _user.id);

//           _authorizedUsers.forEach((_authorizedUser, auIndex) => {
//             this.authorisedUserFullName.push(this.userService.getUserAccessFullName(_authorizedUser.user_ID, _location.id, this.userSession.users, this.userSession.locations));
//           });
//           this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, true, true, this.authorisedUserFullName.join()));
//         });
//       }
//     });









//     // this.events.subscribe('map:dataLoaded', () => {
//     //   // if (!this.currentUser.isAdmin) {
//     //   // let _locationOptions: GeolocationOptions = {
//     //   //   maximumAge: this.SCHEDULER_INTERVAL_IN_MILISEC,
//     //   //   enableHighAccuracy: true
//     //   // };

//     //   // this.watchID = this.geolocation.watchPosition(_locationOptions);
//     //   // this.locationSubscription = this.geolocation.watchPosition(this.watchID).subscribe((position) => {
//     //   //   this.events.publish('map:LocationUpdated',position);
//     //   // });
//     //   // this.scheduler = setTimeout(() => {


//     //   // this.locationWatchStarted = !this.locationWatchStarted;
//     //   // if(this.locationWatchStarted) {
//     //   //   this.locationSubscription.unsubscribe();
//     //   // } else {
//     //   //   this.locationSubscription = this.geolocation.watchPosition(this.watchID).subscribe((position) => {
//     //   //     this.events.publish('map:LocationUpdated',position);
//     //   //   });
//     //   // }
//     //   // }, this.SCHEDULER_INTERVAL_IN_MILISEC);

//     //   // this.liveGeoLocationOnError().then(_error => {
//     //   //   console.log(_error);
//     //   // });
//     //   // } else {
//     //   // this.addMarkers();
//     //   // const source = interval(this.SCHEDULER_INTERVAL_IN_MILISEC);
//     //   // this.scheduler = source.subscribe(val => {
//     //   //   this.updateUI();
//     //   // });
//     //   // }
//     // });
//   }

//   async run() {
//     // this.alert = await this.alertController.create({
//     //   header: 'Message',
//     //   message: 'You are near the office. You can now request access.',
//     //   buttons: [{
//     //     text: 'Ignore',
//     //     role: 'cancel',
//     //     cssClass: 'secondary',
//     //     handler: canceled => {
//     //       console.log('Canceled');
//     //     }
//     //   },
//     //   {
//     //     text: 'Request access',
//     //     role: 'cancel',
//     //     handler: requestedAccess => {
//     //       // let event: Event = {
//     //       //   'status': 'waiting',
//     //       //   'location_ID': this.locationID ,
//     //       //   'user_ID': this.currentUser.uid,
//     //       // }
//     //       // this.eventUserRightsService.addEventUserRight(event);
//     //       console.log('requestedAccess');
//     //     }
//     //   }],
//     // });
//   }

//   updateLocation() {
//     console.log("updateLocation");
//     if (!this.currentUser.isAdmin) {
//       this.geolocation.getCurrentPosition().then(_userLocation => {
//         if (_userLocation != null) {
//           let _updatedUser = {
//             Location: {
//               latitude: _userLocation.coords.latitude,
//               longitude: _userLocation.coords.longitude
//             }
//           };
//           this.userSession.currentUser.Location = _updatedUser.Location;
//           this.currentUser = this.userSession.currentUser;
//           this.db.set(CONSTANTS.USER_SESSION, this.userSession);
//           this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
//             //TODO: Show loading indicator!
//             this.userSession = _userSession;
//             this.userService.updateUser(this.currentUser.uid, _updatedUser).then(() => {
//               //TODO: Hide loading indicator!
//               this.updateUI();
//             });
//           });
//         }
//       });
//     } else {
//       this.updateUI();
//     }
//   }

//   getUserData() {

//   }

//   getUsersData() {

//   }

//   getLocationsData() {
//     this.locationService.getLocations().subscribe(_locations => {
//     });
//   }

//   getData() {
//     if (this.currentUser.isAdmin) {
//       this.getUsersData();
//     } else {
//       this.getUserData();
//     }

//       this.userService.getUser(this.currentUser.uid).subscribe(_updatedUser => {
//         this.db.set(CONSTANTS.CURRENT_USER, _updatedUser);
//         this.db.get(CONSTANTS.USER_SESSION).then(_userSession => {
//           this.userSession = _userSession;
//           this.currentUser = this.userSession.currentUser;
//           this.init();
//           this.userSession.locations.forEach(_location => {
//             this.markers = [];

//             if (this.currentUser.isAdmin) {
//               this.locationService.getLocations().subscribe(_locations => {
//                 let ___locations = [];
//                 _locations.map((_location, lIndex) => {
//                   let _currentLocation = {
//                     id: _location.id,
//                     coords: {
//                       latitude: _location.coords.latitude,
//                       longitude: _location.coords.longitude
//                     },
//                     location_ID: _location.id,
//                     name: _location.name,
//                     hasAccess: {}
//                   };
//                   if (lIndex === _locations.length - 1) {
//                     this.userService.getUsers().subscribe(_users => {
//                       _userSession.users = _users;

//                       this.userSession.users.forEach((_user, uIndex) => {

//                         this.locationService.hasAccess(_user.id, _currentLocation.location_ID).subscribe(_permission => {
//                           _currentLocation.hasAccess = _permission;
//                           ___locations.push(_currentLocation);

//                           this.userRightsService.getUserRights().subscribe(_userRights => {
//                             if (!_user.isAdmin) {
//                               let _authorizedUsers = _userRights.filter((right) => right.permission == true && right.location_ID == _location.id && right.user_ID == _user.id);
//                               _authorizedUsers.forEach(_authorizedUser => {
//                                 if (this.authorisedUserFullName.length <= _userSession.users.length) {
//                                   this.authorisedUserFullName.push(this.userService.getUserAccessFullName(_authorizedUser.user_ID, _location.id, this.userSession.users, this.userSession.locations));
//                                 }
//                               });

//                               let _name = _user.firstName + ' ' + _user.lastName;
//                               let userMarker = this.mapService.createMarker(this.map, _user.Location, true, true, true, _name);
//                               if (this.authorisedUserFullName.length <= _userSession.users.length) {
//                                 this.markers.push(userMarker);
//                               }
//                             }
//                             if (uIndex === this.userSession.users.length - 1) {
//                               this.userSession = _userSession;
//                               this.db.set(CONSTANTS.USER_SESSION, _userSession);    
//                               _userSession.locations = ___locations;          
//                               this.db.set(CONSTANTS.LOCATIONS, ___locations);
//                               if (this.authorisedUserFullName.length <= _userSession.users.length) {
//                                 this.markers.push(this.mapService.createMarker(this.map, _location.coords, false, true, true, this.authorisedUserFullName.join()));
//                               }
//                               this.accessGranted = true;
//                               //TODO: Hide loading indicator
//                               //this.loading.dismiss();
//                               // this.scheduler = setInterval(() => {
//                               //   clearInterval(this.scheduler);
//                               //   this.updateLocation();
//                               // }, this.SCHEDULER_INTERVAL_IN_MILISEC);
//                               // this.events.publish('user:loggedIn', _userSession);
//                               this.setMapOnAll(this.map);
//                               this.scheduler = setInterval(() => {
//                                  clearInterval(this.scheduler);
//                                  this.updateLocation();
//                                }, this.SCHEDULER_INTERVAL_IN_MILISEC);
//                               // this.events.publish('map:dataLoaded');
//                             }
//                           });
//                         });
//                       });
//                     });
//                   }
//                 });
//               });
//             } else {
//               this.accessGranted = this.userSession.locations.every(_location => _location.hasAccess === true);
//               let userMarker = this.mapService.createMarker(this.map, this.currentUser.Location, true, this.currentUser.isAdmin, _location.hasAccess);
//               let officeMarker = this.mapService.createMarker(this.map, _location.coords, false, this.currentUser.isAdmin, _location.hasAccess);
//               // if (this.mapService.isUserAround(userMarker, officeMarker)) {
//               //TODO: Popup that can request access!
//               // }
//               this.markers.push(userMarker);
//               this.markers.push(officeMarker);
//               this.setMapOnAll(this.map);
//               // this.events.publish('map:dataLoaded');
//             }
//           });
//         });
//       });
//   }

//   // var liveGeoLocationOnSuccess = function(position?) {
//   //   MapPage.updateMyLocation(position);
//   // }


//   // liveGeoLocationOnError(error?): Promise<any> {
//   //   return new Promise((reject) => {
//   //     reject(error);
//   //   });
//   // }

//   stopWatchingForLocation() {
//     if (this.watchID != null) {
//       navigator.geolocation.clearWatch(this.watchID);
//     }
//   }
//   setMapOnAll(map) {
//     for (var i = 0; i < this.markers.length; i++) {
//       this.markers[i].setMap(map);
//     }
//   }

//   clearMarkers() {
//     this.setMapOnAll(null);
//   }

//   deleteMarkers() {
//     this.clearMarkers();
//     this.markers = [];
//   }

//   updateUI() {
//     this.deleteMarkers();
//     this.getData();

//     // this.myTimeout = setTimeout(() => {
//     //   //TODO: Update each two seconds1
//     // }, 2000);

//     // this.locationService.liveGeoLocation().subscribe((liveData) => {
//     //   console.log(liveData);
//     // });
//   }
//   async addEvent() {
//     // const alert = await this.alertController.create({
//     //   header: 'You are about to request access. Continue?',
//     //   buttons: [
//     //     {
//     //       text: 'Cancel',
//     //       role: 'cancel',
//     //       cssClass: 'secondary',
//     //       handler: (blah) => {
//     //         console.log('Confirm Cancel');
//     //       }
//     //     }, {
//     //       text: 'Yes',
//     //       handler: () => {
//     //         // let event: Event = {
//     //         //   'status': 'waiting',
//     //         //   'location_ID': this.locationID ,
//     //         //   'user_ID': this.currentUser.uid,
//     //         // }
//     //         // this.eventUserRightsService.addEventUserRight(event);
//     //         console.log('Confirm Okay');
//     //       }
//     //     }
//     //   ]
//     // });

//     // await alert.present();
//   }

//   logout() {
//     this.authService.logoutUser(this.currentUser)
//       .then(res => {
//         console.log(res);
//         this.router.navigate(['/']);
//       });
//   }
// }