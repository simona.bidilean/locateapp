import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable()
export class MyAccountResolver implements Resolve<any> {

  constructor(private userService: UserService) {}

  resolve() {
    
  }
}
