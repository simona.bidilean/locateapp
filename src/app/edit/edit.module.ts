import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserSessionResolver } from '../resolver/user.session.resolver';
import { EditPageRoutingModule } from './edit-routing.module';
import { EditPage } from './edit.page';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '/edit',
        component: EditPage,
        resolve: {
          UserSessionResolver
        }
      }
    ]),
    EditPageRoutingModule
  ],
  declarations: [EditPage],
  providers: [
    UserSessionResolver
  ]
})
export class EditPageModule {}
