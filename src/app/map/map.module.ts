import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MapPage } from './map.page';
import { MapPageRoutingModule } from './map-routing.module';
import { UserSessionResolver } from '../resolver/user.session.resolver';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '/map',
        component: MapPage,
        resolve: {
          UserSessionResolver
        }
      }
    ]),
    MapPageRoutingModule
  ],
  declarations: [MapPage], 
  providers: [
    UserSessionResolver
  ]
})
export class MapPageModule {}
