import { Injectable } from '@angular/core';
declare var google;

@Injectable({
  providedIn: 'root'
})
export class MapService {
  constructor() {
  }

  createMarker(mapWrapper, position, isMyLocation, isAdmin, accessGranted?, description?): any {
    let _color = (isMyLocation) ? 'yellow' : (isAdmin) ? 'green' : 'red';
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(position.latitude, position.longitude),
      icon: {
        url: 'https://maps.google.com/mapfiles/ms/icons/'+ _color  +'-dot.png'
      },
      scaledSize: new google.maps.Size(40, 40)
    });

    if (!isMyLocation) {
      delete marker.icon;
    }

    let infoWindow = new google.maps.InfoWindow({
      content: (isAdmin) ? description : (accessGranted) ? (isMyLocation) ? "Your current location" : "Access granted" : "Access denied"
    });

    google.maps.event.addListener(marker, 'click', function () {
      infoWindow.open(mapWrapper, marker);
    });

    return marker;
  }



  isUserAround(userLocation, officeLocation){
    debugger;
    let R = 6378137;
    let dLat = this.degreesToRadians(officeLocation.latitude - userLocation.latitude);
    let dLong = this.degreesToRadians(officeLocation.longitude - userLocation.longitude);
    let a = Math.sin(dLat / 2)
            *
            Math.sin(dLat / 2) 
            +
            Math.cos(this.degreesToRadians(userLocation.latitude)) 
            * 
            Math.cos(this.degreesToRadians(userLocation.latitude)) 
            *
            Math.sin(dLong / 2) 
            * 
            Math.sin(dLong / 2);

    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return (d<500) 
    
  }

      degreesToRadians(degrees){
        return degrees * Math.PI / 180;
    }

}