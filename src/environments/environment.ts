// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCY0d2XInTrXIEQA6zn0ZFsCUp68469ElQ",
  authDomain: "locateapp-a166f.firebaseapp.com",
  databaseURL: "https://locateapp-a166f.firebaseio.com",
  projectId: "locateapp-a166f",
  storageBucket: "locateapp-a166f.appspot.com",
  messagingSenderId: "51516981304",
  appId: "1:51516981304:web:c9fe54abee587e343e26cd",
  measurementId: "G-ZR0XNX4HPG"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
