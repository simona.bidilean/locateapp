(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-account-my-account-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/my-account/my-account.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-account/my-account.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>myAccount</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-grid>\r\n    <ion-row id=\"row1\">\r\n      <ion-col text-center>\r\n         <ion-icon name=\"ios-contact\" id=\"person\"></ion-icon> <br>\r\n      <h1> {{currentUser.firstName}} {{currentUser.lastName}} </h1>\r\n      <h5> {{currentUser.function}}</h5>\r\n    </ion-col>\r\n    </ion-row>\r\n  </ion-grid>  \r\n\r\n\r\n  <ion-row id=\"row2\">\r\n    <ion-col col-12>\r\n      <ion-card padding>\r\n\r\n       <ion-icon name=\"mail\" class=\"icons\"> </ion-icon> <h4>{{currentUser.email}}</h4>   \r\n        <br> <br>\r\n        <ion-icon name=\"call\" class=\"icons\" ></ion-icon> <h4>{{currentUser.phone}}</h4> \r\n        <br> <br>\r\n        <ion-icon name=\"home\" class=\"icons\"></ion-icon>  <h4>{{currentUser.address}}</h4> \r\n\r\n      </ion-card>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n  <ion-button (click)=\"deleteAccount()\" color=\"dark\">\r\n    Delete account\r\n  </ion-button>\r\n  <ion-button routerLink=\"/edit\" routerDirection=\"root\" color=\"dark\">\r\n    Update Profile ->\r\n  </ion-button>\r\n\r\n</ion-content>\r\n\r\n<ion-footer>\r\n  <ion-row>\r\n    <ion-col text-center>\r\n  <ion-button (click)=\"logout()\" color=\"dark\">\r\n    Log out\r\n  </ion-button>\r\n</ion-col>\r\n</ion-row>\r\n</ion-footer>"

/***/ }),

/***/ "./src/app/my-account/my-account-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/my-account/my-account-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: MyAccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAccountPageRoutingModule", function() { return MyAccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _my_account_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-account.page */ "./src/app/my-account/my-account.page.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");





var routes = [
    {
        path: '',
        component: _my_account_page__WEBPACK_IMPORTED_MODULE_3__["MyAccountPage"],
        resolve: {
            data: _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_4__["UserSessionResolver"]
        }
    }
];
var MyAccountPageRoutingModule = /** @class */ (function () {
    function MyAccountPageRoutingModule() {
    }
    MyAccountPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], MyAccountPageRoutingModule);
    return MyAccountPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/my-account/my-account.module.ts":
/*!*************************************************!*\
  !*** ./src/app/my-account/my-account.module.ts ***!
  \*************************************************/
/*! exports provided: MyAccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAccountPageModule", function() { return MyAccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _my_account_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-account-routing.module */ "./src/app/my-account/my-account-routing.module.ts");
/* harmony import */ var _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../resolver/user.session.resolver */ "./src/app/resolver/user.session.resolver.ts");
/* harmony import */ var _my_account_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./my-account.page */ "./src/app/my-account/my-account.page.ts");








var MyAccountPageModule = /** @class */ (function () {
    function MyAccountPageModule() {
    }
    MyAccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _my_account_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyAccountPageRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_my_account_page__WEBPACK_IMPORTED_MODULE_7__["MyAccountPage"]],
            providers: [
                _resolver_user_session_resolver__WEBPACK_IMPORTED_MODULE_6__["UserSessionResolver"]
            ]
        })
    ], MyAccountPageModule);
    return MyAccountPageModule;
}());



/***/ }),

/***/ "./src/app/my-account/my-account.page.scss":
/*!*************************************************!*\
  !*** ./src/app/my-account/my-account.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#person {\n  width: 100px;\n  height: 100px;\n}\n\n#row1 {\n  background-color: #93ccb7;\n}\n\n.icons {\n  width: 60px;\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktYWNjb3VudC9DOlxcVXNlcnNcXHNpbW9uYS5iaWRpbGVhblxcRG9jdW1lbnRzXFxsb2NhdGVBcHBcXGxvY2F0ZWFwcC9zcmNcXGFwcFxcbXktYWNjb3VudFxcbXktYWNjb3VudC5wYWdlLnNjc3MiLCJzcmMvYXBwL215LWFjY291bnQvbXktYWNjb3VudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQ0NKOztBREVBO0VBQ0UseUJBQUE7QUNDRjs7QURHQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQUoiLCJmaWxlIjoic3JjL2FwcC9teS1hY2NvdW50L215LWFjY291bnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3BlcnNvbntcclxuICAgIHdpZHRoOjEwMHB4IDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbn1cclxuXHJcbiNyb3cxe1xyXG4gIGJhY2tncm91bmQtY29sb3I6cmdiKDE0NywgMjA0LCAxODMpO1xyXG59XHJcblxyXG5cclxuLmljb25ze1xyXG4gICAgd2lkdGg6IDYwcHggO1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG59XHJcblxyXG4iLCIjcGVyc29uIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4jcm93MSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5M2NjYjc7XG59XG5cbi5pY29ucyB7XG4gIHdpZHRoOiA2MHB4O1xuICBoZWlnaHQ6IDYwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/my-account/my-account.page.ts":
/*!***********************************************!*\
  !*** ./src/app/my-account/my-account.page.ts ***!
  \***********************************************/
/*! exports provided: MyAccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAccountPage", function() { return MyAccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var firebase_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase/auth */ "./node_modules/firebase/auth/dist/index.esm.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");












var MyAccountPage = /** @class */ (function () {
    function MyAccountPage(router, route, alertController, userService, navCtrl, authService, formBuilder, db) {
        var _this = this;
        this.router = router;
        this.route = route;
        this.alertController = alertController;
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.db = db;
        this.route.data.subscribe(function (session) {
            _this.userSession = session.data;
            _this.currentUser = _this.userSession.currentUser;
        });
    }
    MyAccountPage.prototype.deleteAccount = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Are you sure of this? ',
                            message: 'Your account will be deleted and you will be logged out.',
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Yes',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                        _this.userService.deleteUser(_this.currentUser.id).then(function (resepnose) {
                                            console.log(resepnose);
                                            _this.logout();
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyAccountPage.prototype.logout = function () {
        var _this = this;
        this.authService.logoutUser(this.currentUser)
            .then(function (res) {
            console.log(res);
            _this.db.clear().then(function () {
                _this.router.navigate(['/']);
            });
        });
    };
    MyAccountPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"] }
    ]; };
    MyAccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-account',
            template: __webpack_require__(/*! raw-loader!./my-account.page.html */ "./node_modules/raw-loader/index.js!./src/app/my-account/my-account.page.html"),
            styles: [__webpack_require__(/*! ./my-account.page.scss */ "./src/app/my-account/my-account.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_9__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticateService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"]])
    ], MyAccountPage);
    return MyAccountPage;
}());



/***/ }),

/***/ "./src/app/resolver/user.session.resolver.ts":
/*!***************************************************!*\
  !*** ./src/app/resolver/user.session.resolver.ts ***!
  \***************************************************/
/*! exports provided: UserSessionResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSessionResolver", function() { return UserSessionResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants/constants */ "./src/app/constants/constants.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/user-rights.service */ "./src/app/services/user-rights.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");











var UserSessionResolver = /** @class */ (function () {
    function UserSessionResolver(db, geolocation, userService, locationService, userRightsService, events) {
        this.db = db;
        this.geolocation = geolocation;
        this.userService = userService;
        this.locationService = locationService;
        this.userRightsService = userRightsService;
        this.events = events;
    }
    UserSessionResolver.prototype.resolve = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.db.get(_constants_constants__WEBPACK_IMPORTED_MODULE_4__["USER_SESSION"]).then(function (_userSession) {
                if (_userSession != null) {
                    if (_userSession.currentUser != null) {
                        if (_userSession.currentUser.id != null) {
                            _this.userService.getUser(_userSession.currentUser.id).subscribe(function (userDetail) {
                                _userSession.currentUser = userDetail;
                                _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser).then(function () {
                                    if (!_userSession.currentUser.isAdmin) {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.geolocation.getCurrentPosition(), _this.locationService.getLocations(), _this.userRightsService.getUserRightsByUserId(_userSession.currentUser.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var mylocation = _a[0], locations = _a[1], userRights = _a[2];
                                            return ({ mylocation: mylocation, locations: locations, userRights: userRights });
                                        })).subscribe(function (response) {
                                            _userSession.currentUser.Location.latitude = response['mylocation'].coords.latitude;
                                            _userSession.currentUser.Location.longitude = response['mylocation'].coords.longitude;
                                            _this.userService.updateUser(_userSession.currentUser.uid, _userSession.currentUser);
                                            delete response['mylocation'];
                                            response['users'] = [_userSession.currentUser];
                                            response['usersRight'] = [];
                                            response['usersRight'].push(response['userRights']);
                                            delete response['userRights'];
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    var _permission = response['usersRight'].filter(function (accessRight) { return accessRight.user_ID == _userSession.currentUser.id && accessRight.location_ID == _location.id; })[0].permission;
                                                    if (_permission) {
                                                        _location.hasAccess = _permission;
                                                    }
                                                    else if (!_permission || !response['usersRight']) {
                                                        _location.hasAccess = false;
                                                    }
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(_this.locationService.getLocations(), _this.userService.getUsers(), _this.userRightsService.getUserRights()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                                            var locations = _a[0], users = _a[1], usersRight = _a[2];
                                            return ({ locations: locations, users: users, usersRight: usersRight });
                                        })).subscribe(function (response) {
                                            response['currentUser'] = _userSession.currentUser;
                                            var _updatedLocations = [];
                                            response['locations'].forEach(function (_location, lIndex) {
                                                response['users'].forEach(function (_user, uIndex) {
                                                    _location.hasAccess = _this.locationService.hasAccess(_userSession.currentUser.id, _location.location_ID, response.usersRight);
                                                    if (_updatedLocations.length < response['locations'].length) {
                                                        _updatedLocations.push(_location);
                                                    }
                                                    if ((uIndex === response['users'].length - 1) && (lIndex === response['locations'].length - 1)) {
                                                        response['locations'] = _updatedLocations;
                                                        _this.events.publish('user:loggedIn', response);
                                                        resolve(response);
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }
                }
            });
        });
    };
    UserSessionResolver.ctorParameters = function () { return [
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] },
        { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
        { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
        { type: _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"] }
    ]; };
    UserSessionResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
            _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"],
            _services_user_rights_service__WEBPACK_IMPORTED_MODULE_9__["UserRightsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Events"]])
    ], UserSessionResolver);
    return UserSessionResolver;
}());



/***/ })

}]);
//# sourceMappingURL=my-account-my-account-module-es5.js.map