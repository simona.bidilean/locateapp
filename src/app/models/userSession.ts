import { User } from '../models/user';
import { OfficeLocation } from './location';
import { UserRight } from './userRight';

export class UserSession {
    currentUser?: User = null;
    locations: OfficeLocation[] = [];
    users: User[] = [];
    usersRight: UserRight[] = [];
    token: any;
}