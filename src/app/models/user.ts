export class User {
    Location?:{
      latitude:number;
      longitude:number;
    };
    firstName?: string;
    lastName?: string;
    phone?: string;
    email?: string;
    address?: string;
    isAdmin?: boolean;
    function?:string;
    id?:string;
    uid?:string;
    isOnline?:boolean;
  }