import { Injectable, AfterViewInit } from '@angular/core';
import 'firebase/auth';
import 'firebase/firestore';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/storage';
import { OfficeLocation } from '../models/location';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UserRightsService } from './user-rights.service';
import { UserRight } from '../models/userRight';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private locations: Observable<OfficeLocation[]>;
  private locationCollection: AngularFirestoreCollection<OfficeLocation>;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    private geolocation: Geolocation,
    private userRightsService: UserRightsService,
  ) {
    this.locationCollection = this.afs.collection<OfficeLocation>('locations');
    this.locations = this.locationCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getMyGeoLocation() {
    return this.geolocation.getCurrentPosition();
  }

  getLocations(): Observable<OfficeLocation[]> {
    return this.locations;
  }

  getLocation(id: string): Observable<OfficeLocation> {
    return this.locationCollection.doc<OfficeLocation>(id).valueChanges().pipe(
      take(1),
      map(location => {
        location.id = id;
        return location
      })
    );
  }

  updateLocation(location: OfficeLocation): Promise<void> {
    return this.locationCollection.doc(location.id).update({ latitude: location.coords.latitude, longitude: location.coords.longitude, location_ID: location.location_ID, name: location.name });
  }

  addLocation(location: OfficeLocation): Promise<DocumentReference> {
    return this.locationCollection.add(location);
  }

  hasAccess(userId: string, locationId: string, userRights: UserRight[]): boolean {
    let _filteredUR = userRights.filter((accessRight) => accessRight.user_ID == userId && accessRight.location_ID == locationId);
    if (_filteredUR.length > 0) {
      return _filteredUR[0].permission;
    } else if(_filteredUR.length==0 || !_filteredUR || !_filteredUR[0].permission) {
      return false;
    }
  }

  getAuthorisedUsers(locationId: string, userId: string): Observable<UserRight[]> {
    return this.userRightsService.getUserRights().pipe(map(rights => {
      return rights.filter((right) => right.permission == true && right.location_ID == locationId && right.user_ID == userId);
    }));
  }

  deleteLocation(id: string): Promise<void> {
    return this.locationCollection.doc(id).delete();
  }

  getCompanyNameById(locationId:string, locations: OfficeLocation[]){
    for(var i = 0; i< locations.length; i++){
      if(locations[i].id == locationId){
        return locations[i].name;
      }
    }
  }

}