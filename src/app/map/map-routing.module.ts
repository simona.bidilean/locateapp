import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapPage } from './map.page';
import { UserSessionResolver } from '../resolver/user.session.resolver';

const routes: Routes = [
  {
    path: '',
    component: MapPage,
    resolve: {
      data: UserSessionResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapPageRoutingModule {}
